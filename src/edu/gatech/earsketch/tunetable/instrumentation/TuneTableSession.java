package edu.gatech.earsketch.tunetable.instrumentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import edu.gatech.earsketch.tunetable.application.TuneTable.Block;
import edu.gatech.earsketch.tunetable.application.TuneTable.Lead;
import edu.gatech.earsketch.tunetable.io.json.JSONUtilities;

public class TuneTableSession
{
	private JSONSession currentSession;
	
	public TuneTableSession()
	{
		currentSession = new JSONSession();
		currentSession.setSessionID(UUID.randomUUID());
	}
	
	public void addGraphState(ArrayList<Block> blocks)
	{
		GraphState currentGraphState = new GraphState();
		
		for(Block block : blocks)
		{
			BlockState blockState = new BlockState(block.sym_id, block.type, false, block.x_pos, block.y_pos, block.rotation);
			currentGraphState.getBlocks().add(blockState);
			
			for(Lead lead : block.leads)
			{
				LeadState leadState = null;
				if(lead.occupant != null)
				{
					leadState = new LeadState(block.sym_id + ":" + lead.leadIndex, lead.occupied, lead.owner.sym_id, lead.occupant.sym_id);
				}
				else
				{
					leadState = new LeadState(block.sym_id + ":" + lead.leadIndex, lead.occupied, lead.owner.sym_id, -1);
				}
				currentGraphState.getLeads().add(leadState);
			}
		}
		
		long timeStamp = System.currentTimeMillis();
		currentGraphState.setTimeStamp(timeStamp);
		
		currentSession.getGraphStates().add(currentGraphState);
		
		printCurrentSession();
	}
	
	public void addSoundBankState(HashMap<Integer, String[]> clipDictionary)
	{
		SoundBankState currentSoundBankState = new SoundBankState();
		
		for(String[] clipSet : clipDictionary.values())
		{
			for(String clip : clipSet)
			{
				ClipState clipState = new ClipState(clip, false);
				currentSoundBankState.getClips().add(clipState);
			}
		}
		
		long timeStamp = System.currentTimeMillis();
		currentSoundBankState.setTimeStamp(timeStamp);

		currentSession.getSoundBankStates().add(currentSoundBankState);
		
		printCurrentSession();
	}
	
	public void printCurrentSession()
	{
		System.out.println("Current Session State (JSON):");
		System.out.println(JSONUtilities.convertObjectToJSONString(this.currentSession, JSONSession.class));
	}
}
