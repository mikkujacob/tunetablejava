package edu.gatech.earsketch.tunetable.instrumentation;

import java.util.ArrayList;
import java.util.Arrays;

public class GraphState
{
	private long timeStamp;
	private ArrayList<BlockState> blocks;
	private ArrayList<LeadState> leads;
	
	public GraphState()
	{
		this.timeStamp = 0;
		this.blocks = new ArrayList<BlockState>();
		this.leads = new ArrayList<LeadState>();
	}
	
	public GraphState(long timeStamp, ArrayList<BlockState> blocks, ArrayList<LeadState> leads)
	{
		this.timeStamp = timeStamp;
		this.blocks = new ArrayList<BlockState>(blocks);
		this.leads = new ArrayList<LeadState>(leads);
	}
	
	public GraphState(long timeStamp, BlockState[] blocks, LeadState[] leads)
	{
		this.timeStamp = timeStamp;
		this.blocks = new ArrayList<BlockState>(Arrays.asList(blocks));
		this.leads = new ArrayList<LeadState>(Arrays.asList(leads));
	}
	
	/**
	 * @return the timeStamp
	 */
	public long getTimeStamp()
	{
		return timeStamp;
	}
	
	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(long timeStamp)
	{
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the blocks
	 */
	public ArrayList<BlockState> getBlocks()
	{
		return blocks;
	}

	/**
	 * @param blocks the blocks to set
	 */
	public void setBlocks(ArrayList<BlockState> blocks)
	{
		this.blocks = blocks;
	}

	/**
	 * @return the leads
	 */
	public ArrayList<LeadState> getLeads()
	{
		return leads;
	}

	/**
	 * @param leads the leads to set
	 */
	public void setLeads(ArrayList<LeadState> leads)
	{
		this.leads = leads;
	}
}