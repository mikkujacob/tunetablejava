package edu.gatech.earsketch.tunetable.instrumentation;

public class ClipState
{
	private String clipName;
	private boolean loop;
	
	public ClipState()
	{
		clipName = "";
		loop = false;
	}
	
	public ClipState(String clipName, boolean loop)
	{
		this.clipName = clipName;
		this.loop = loop;
	}
	
	/**
	 * @return the clipName
	 */
	public String getClipName()
	{
		return clipName;
	}
	
	/**
	 * @param clipName the clipName to set
	 */
	public void setClipName(String clipName)
	{
		this.clipName = clipName;
	}
	
	/**
	 * @return the loop
	 */
	public boolean isLoop()
	{
		return loop;
	}
	
	/**
	 * @param loop the loop to set
	 */
	public void setLoop(boolean loop)
	{
		this.loop = loop;
	}
}