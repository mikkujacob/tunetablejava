package edu.gatech.earsketch.tunetable.instrumentation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

public class JSONSession
{
	private UUID sessionID;
	private ArrayList<GraphState> graphStates;
	private ArrayList<SoundBankState> soundBankStates;
	
	public JSONSession()
	{
		sessionID = null;
		graphStates = new ArrayList<GraphState>();
		soundBankStates = new ArrayList<SoundBankState>();
	}
	
	public JSONSession(UUID sessionID, ArrayList<GraphState> graphStates, ArrayList<SoundBankState> soundBankStates)
	{
		this.sessionID = sessionID;
		this.graphStates = new ArrayList<GraphState>(graphStates);
		this.soundBankStates = new ArrayList<SoundBankState>(soundBankStates);
	}
	
	public JSONSession(String sessionID, GraphState[] graphStates, SoundBankState[] soundBankStates)
	{
		this.sessionID = UUID.fromString(sessionID);
		this.graphStates = new ArrayList<GraphState>(Arrays.asList(graphStates));
		this.soundBankStates = new ArrayList<SoundBankState>(Arrays.asList(soundBankStates));
	}
	
	/**
	 * @return the sessionID
	 */
	public UUID getSessionID()
	{
		return sessionID;
	}
	
	/**
	 * @param sessionID the sessionID to set
	 */
	public void setSessionID(UUID sessionID)
	{
		this.sessionID = sessionID;
	}

	/**
	 * @return the graphStates
	 */
	public ArrayList<GraphState> getGraphStates()
	{
		return graphStates;
	}

	/**
	 * @param graphStates the graphStates to set
	 */
	public void setGraphStates(ArrayList<GraphState> graphStates)
	{
		this.graphStates = graphStates;
	}

	/**
	 * @return the soundBankStates
	 */
	public ArrayList<SoundBankState> getSoundBankStates()
	{
		return soundBankStates;
	}

	/**
	 * @param soundBankStates the soundBankStates to set
	 */
	public void setSoundBankStates(ArrayList<SoundBankState> soundBankStates)
	{
		this.soundBankStates = soundBankStates;
	}
}