package edu.gatech.earsketch.tunetable.instrumentation;

public class LeadState
{
	private String leadID;
	private boolean hasDestination;
	private int originID;
	private int destinationID;
	
	public LeadState()
	{
		leadID = "";
		hasDestination = false;
		originID = -1;
		destinationID = -1;
	}
	
	public LeadState(String leadID, boolean hasDestination, int originID, int destinationID)
	{
		this.leadID = leadID;
		this.hasDestination = hasDestination;
		this.originID = originID;
		this.destinationID = destinationID;
	}
	
	/**
	 * @return the leadID
	 */
	public String getLeadID()
	{
		return leadID;
	}
	
	/**
	 * @param leadID the leadID to set
	 */
	public void setLeadID(String leadID)
	{
		this.leadID = leadID;
	}
	
	/**
	 * @return the hasDestination
	 */
	public boolean hasDestination()
	{
		return hasDestination;
	}
	
	/**
	 * @param hasDestination the hasDestination to set
	 */
	public void setHasDestination(boolean hasDestination)
	{
		this.hasDestination = hasDestination;
	}
	
	/**
	 * @return the originID
	 */
	public int getOriginID()
	{
		return originID;
	}
	
	/**
	 * @param originID the originID to set
	 */
	public void setOriginID(int originID)
	{
		this.originID = originID;
	}
	
	/**
	 * @return the destinationID
	 */
	public int getDestinationID()
	{
		return destinationID;
	}
	
	/**
	 * @param destinationID the destinationID to set
	 */
	public void setDestinationID(int destinationID)
	{
		this.destinationID = destinationID;
	}
}