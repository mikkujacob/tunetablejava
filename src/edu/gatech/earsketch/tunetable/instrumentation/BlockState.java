package edu.gatech.earsketch.tunetable.instrumentation;

import edu.gatech.earsketch.tunetable.application.TuneTable.BlockType;

public class BlockState
{
	private int blockID;
	private BlockType blockType;
	private boolean isPlayHead;
	private float xPos;
	private float yPos;
	private float rotation;
	
	public BlockState()
	{
		this.blockID = -1;
		this.blockType = null;
		this.isPlayHead = false;
		this.xPos = 0;
		this.yPos = 0;
		this.rotation = 0;
	}
	
	public BlockState(int blockID, BlockType blockType, boolean isPlayHead, float xPos, float yPos, float rotation)
	{
		this.blockID = blockID;
		this.blockType = blockType;
		this.isPlayHead = isPlayHead;
		this.xPos = xPos;
		this.yPos = yPos;
		this.rotation = rotation;
	}
	
	/**
	 * @return the blockID
	 */
	public int getBlockID()
	{
		return blockID;
	}
	
	/**
	 * @param blockID the blockID to set
	 */
	public void setBlockID(int blockID)
	{
		this.blockID = blockID;
	}
	
	/**
	 * @return the blockType
	 */
	public BlockType getBlockType()
	{
		return blockType;
	}
	
	/**
	 * @param blockType the blockType to set
	 */
	public void setBlockType(BlockType blockType)
	{
		this.blockType = blockType;
	}
	
	/**
	 * @return the isPlayHead
	 */
	public boolean isPlayHead()
	{
		return isPlayHead;
	}
	
	/**
	 * @param isPlayHead the isPlayHead to set
	 */
	public void setPlayHead(boolean isPlayHead)
	{
		this.isPlayHead = isPlayHead;
	}
	
	/**
	 * @return the xPos
	 */
	public float getxPos()
	{
		return xPos;
	}
	
	/**
	 * @param xPos the xPos to set
	 */
	public void setxPos(float xPos)
	{
		this.xPos = xPos;
	}
	
	/**
	 * @return the yPos
	 */
	public float getyPos()
	{
		return yPos;
	}
	
	/**
	 * @param yPos the yPos to set
	 */
	public void setyPos(float yPos)
	{
		this.yPos = yPos;
	}
	
	/**
	 * @return the rotation
	 */
	public float getRotation()
	{
		return rotation;
	}
	
	/**
	 * @param rotation the rotation to set
	 */
	public void setRotation(float rotation)
	{
		this.rotation = rotation;
	}
}