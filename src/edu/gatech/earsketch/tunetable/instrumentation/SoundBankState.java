package edu.gatech.earsketch.tunetable.instrumentation;

import java.util.ArrayList;
import java.util.Arrays;

public class SoundBankState
{
	private long timeStamp;
	private ArrayList<ClipState> clips;
	
	public SoundBankState()
	{
		timeStamp = -1;
		clips = new ArrayList<>();
	}
	
	public SoundBankState(long timeStamp, ArrayList<ClipState> clips)
	{
		this.timeStamp = timeStamp;
		this.clips = new ArrayList<>(clips);
	}
	
	public SoundBankState(long timeStamp, ClipState[] clips)
	{
		this.timeStamp = timeStamp;
		this.clips = new ArrayList<ClipState>(Arrays.asList(clips));
	}
	
	/**
	 * @return the timeStamp
	 */
	public long getTimeStamp()
	{
		return timeStamp;
	}
	
	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(long timeStamp)
	{
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the clips
	 */
	public ArrayList<ClipState> getClips()
	{
		return clips;
	}

	/**
	 * @param clips the clips to set
	 */
	public void setClips(ArrayList<ClipState> clips)
	{
		this.clips = clips;
	}
}