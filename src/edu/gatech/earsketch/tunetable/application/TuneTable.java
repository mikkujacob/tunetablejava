package edu.gatech.earsketch.tunetable.application;
import processing.core.*;

import ddf.minim.*;
import ddf.minim.ugens.*;
import edu.gatech.earsketch.tunetable.instrumentation.TuneTableSession;

import java.util.concurrent.*;

import TUIO.*;
import java.util.*;

import java.util.HashMap;
import java.util.ArrayList;
import java.io.PrintWriter;

public class TuneTable extends PApplet
{
	// we need to import the TUIO library
	// and declare a TuioProcessing client variable
	// set table resolution to 1280x960
	// Creates Variable and junk
	Minim minim;
	AudioOutput out;
	FilePlayer filePlayer;
	Delay myDelay;

	boolean debug = true;
	boolean invertColor = true;
	boolean showFPS = true;
	boolean hoverDebug = true;
	boolean fullscreen = true;
	boolean analyticsOn = false;
	boolean simulateBlocks = false;
	boolean paused = false;

	// these are some helper variables which are used
	// to create scalable graphical feedback
	static float cursor_size = 15;
	static int block_diameter = 130;
	int text_size;

	// used for beat calculations
	int bpm = 60;
	int beatsPerMeasure = 4;
	int millisPerBeat;
	int beatNo = 0;

	PFont font;

	PImage lock;
	PImage unlock;
	PImage lock_reg;
	PImage unlock_reg;
	PImage lock_inv;
	PImage unlock_inv;
	PShape beatShadow;
	PShape dashCircle;
	PShape playShadow;
	PShape circleShadow;

	List<Block> allBlocks;
	List<Block> missingBlocks;
	LinkedList<Block> killBlocks;

	List<Cursor> cursors;

	List<FunctionBlock> allFunctionBlocks;
	List<Button> allButtons;
	List<PlayHead> allPlayHeads;
	LinkedList<PlayHead> killPlayHeads;

	Cursor mouse;

	ClipBlock testClip;

	boolean isInitiated = false;
	
	TuneTableSession tuneTableSession;
	
	public static void main(String[] passedArgs)
	{
		String[] appletArgs = new String[] { "--present",
				"--window-color=#666666", "--hide-stop", "edu.gatech.earsketch.tunetable.application.TuneTable" };
		if (passedArgs != null)
		{
			PApplet.main(concat(appletArgs, passedArgs));
		}
		else
		{
			PApplet.main(appletArgs);
		}
	}

	public void settings()
	{
		size(displayWidth, displayHeight, P2D);
	}

	public void setup()
	{

		// size(displayWidth, displayHeight, P2D);

		noStroke();
		fill(0);

		loop();
		frameRate(60);

		font = createFont("Arial", 32);
		text_size = block_diameter / 3;

		// SHAPE Setup
		beatShadow = sinCircle(0, 0, block_diameter / 2, 0, 8,
				block_diameter / 20);
		dashCircle = dashedCircle(0, 0, block_diameter, 10);
		playShadow = polygon(block_diameter * .62f, 6);
		playShadow.disableStyle();
		ellipseMode(CENTER);
		circleShadow = createShape(ELLIPSE, 0, 0, block_diameter,
				block_diameter);
		circleShadow.disableStyle();

		// we create an instance of the TuioProcessing client
		// since we add "this" class as an argument the TuioProcessing class
		// expects
		tuioClient = new TuioProcessing(this);
		minim = new Minim(this);

		SetupClipDict();
		SetupFuncMap();
		SetupBoolMap();
		SetupIdToType();
		SetupBlockMap();
		SetupCursorMap();
		SetupIdToEffect();

		allBlocks = new ArrayList<Block>();
		missingBlocks = new ArrayList<Block>();
		killBlocks = new LinkedList<Block>();
		allFunctionBlocks = new ArrayList<FunctionBlock>();
		allButtons = new ArrayList<Button>();
		allPlayHeads = new ArrayList<PlayHead>();
		killPlayHeads = new LinkedList<PlayHead>();

		cursors = new ArrayList<Cursor>();

		// ICONS
		float scaleFactor = 1;
		lock_reg = loadImage("images/lock.png");
		lock_inv = loadImage("images/lock_inv.png");
		scaleFactor = ((float) block_diameter / 3.0f) / (float) lock_reg.height;
		lock_reg.resize((int) (lock_reg.width * scaleFactor),
				(int) (lock_reg.height * scaleFactor));
		lock_inv.resize((int) (lock_inv.width * scaleFactor),
				(int) (lock_inv.height * scaleFactor));

		unlock_reg = loadImage("images/unlock.png");
		unlock_inv = loadImage("images/unlock_inv.png");
		scaleFactor = ((float) block_diameter / 3.0f)
				/ (float) unlock_reg.height;
		unlock_reg.resize((int) (unlock_reg.width * scaleFactor),
				(int) (unlock_reg.height * scaleFactor));
		unlock_inv.resize((int) (unlock_inv.width * scaleFactor),
				(int) (unlock_inv.height * scaleFactor));

		lock = (invertColor ? lock_inv : lock_reg);
		unlock = (invertColor ? unlock_inv : unlock_reg);

		isInitiated = true;
		millisPerBeat = 60000 / bpm;

		if (simulateBlocks)
		{
			FunctionBlock funcTest = new FunctionBlock(500, 500, 0);
			FunctionBlock funcTest2 = new FunctionBlock(500, 800, 1);

			// StartLoopBlock testLoop = new StartLoopBlock(700,500);
			testClip = new ClipBlock(380, 300, 10);
			ClipBlock testClip2 = new ClipBlock(900, 800, 11);
			// ClipBlock baitBlock = new ClipBlock(1200,600,12);
			CallBlock callTest = new CallBlock(700, 800);
			// ConditionalBlock testCond = new ConditionalBlock(900,500);
			// BooleanBlock testBool = new BooleanBlock(900, 200);
		}

		CheckForExistingTuioObjects();
		
		tuneTableSession = new TuneTableSession();
		tuneTableSession.addSoundBankState(clipDict);
	}

	public void draw()
	{
		beatNo = (millis() / millisPerBeat);
		background(invertColor ? 0 : 255);
		// cornerBeatGlow();

		if (debug)
		{
			colorMode(RGB);

		}

		textFont(font, 18);
		killRemoved();

		if (!paused)
		{
			TuioUpdate();
		}

		for (Cursor c : cursors)
		{
			c.Update();
		}

		for (Block b : allBlocks)
		{
			b.inChain = false;
			b.cleanLeads();
			if (!(b instanceof FunctionBlock))
				b.blockColor = color(255);
		}

		for (FunctionBlock func : allFunctionBlocks)
		{
			func.startUpdatePath();
		}

		for (Block b : allBlocks)
		{
			if (b.leadsActive)
			{
				b.drawLeads();
			}
		}

		for (PlayHead p : allPlayHeads)
		{
			p.Update();
			p.draw();
		}

		for (Block b : allBlocks)
		{
			b.Update();
			b.draw();
		}

		/*
		 * for (Button b : allButtons) { if (b.isShowing) b.drawButton(); }
		 */

		if (hoverDebug)
		{
			HoverDebug();
		}

		if (showFPS)
		{
			colorMode(RGB);

			textSize(32);
			textAlign(LEFT, TOP);
			fill(255, 0, 0);
			text((int) frameRate, 80, 80);
		}
	}

	// boolean sketchFullScreen() {
	// return (fullscreen);
	// }

	/*
	 * Space can be used to play all Start blocks 'i' can be used to invert
	 * black/white
	 */
	public void keyPressed()
	{
		if (key == ' ')
		{
			println("space " + millis());
			for (FunctionBlock func : allFunctionBlocks)
			{
				func.execute();
			}
		}
		if (key == 'i')
		{
			invertColor = !invertColor;
			if (invertColor)
			{
				lock = lock_inv;
				unlock = unlock_inv;
			}
			else
			{
				lock = lock_reg;
				unlock = unlock_reg;
			}
		}
		if (key == 'p')
		{
			paused = !paused;
		}
	}

	/*
	 * Will cause all Start blocks to play simultaneously, useful for debugging
	 */
	public void Play()
	{
		for (FunctionBlock func : allFunctionBlocks)
		{
			func.execute();
		}
	}

	/*
	 * Mouse clicks will simulate finger cursors, useful for debugging
	 */
	public void mousePressed()
	{
		mouse = new Cursor();
	}

	public void mouseReleased()
	{
		mouse.OnRemove();
	}

	/*
	 * Will create an informative tooltip when the mouse is hovering over a
	 * block, useful for debugging
	 */
	public void HoverDebug()
	{
		Block[] blocks = new Block[allBlocks.size()];
		allBlocks.toArray(blocks); // fill the array
		for (Block b : blocks)
		{
			if (b.IsUnder(mouseX, mouseY))
			{
				Tooltip(new String[] { "symbol id: " + b.sym_id,
						"x: " + b.x_pos, "y: " + b.y_pos,
						"rotation: " + b.rotation, "in chain? " + b.inChain,
						"children: " + Arrays.toString(b.children),
						"parents: " + b.parents.toString() });
			}
		}
	}
	
	public void appendSessionGraph()
	{
		tuneTableSession.addGraphState((ArrayList<Block>) allBlocks);
	}

	/*
	 * Destroys blocks and playhead that have been marked for removal. This
	 * can't be done during Update() calls because it tends to cause a
	 * concurrent modification exception.
	 */
	public void killRemoved()
	{
		while (killBlocks.peek() != null)
		{
			//TODO REMOVE BLOCK EVENT
			Block remBlock = killBlocks.peek();
			System.out.println("REMOVED BLOCK: " + remBlock.type + " at (" + remBlock.x_pos + ", " + remBlock.y_pos + ")");
			
			System.out.println("Appending at killRemoved.REMOVE BLOCK");
			appendSessionGraph();
			for(Lead lead : remBlock.leads)
			{
				System.out.println("REMOVED LEAD: " + lead.leadIndex + " from " + remBlock.type);
				
				System.out.println("Appending at killRemoved.REMOVE LEAD");
				appendSessionGraph();
			}
			killBlocks.pop().Die();
		}
		while (killPlayHeads.peek() != null)
		{
			killPlayHeads.pop().Die();
		}
	}

	/*
	 * Creates pulses in the screen corners to visualize the beat
	 */
	public void cornerBeatGlow()
	{
		float beatPercent = (1.0f - ((float) (millis() % (millisPerBeat))
				/ (float) (millisPerBeat)));
		int glowRadius = (int) (beatPercent * 300);
		int innerCol = color(invertColor ? 0 : 255);
		int outerCol = color(invertColor ? 100 : 150);

		fill(outerCol);
		noStroke();
		ellipse(0, 0, glowRadius, glowRadius);
		ellipse(width, 0, glowRadius, glowRadius);
		ellipse(0, height, glowRadius, glowRadius);
		ellipse(width, height, glowRadius, glowRadius);
	}

	public class BeatBlock extends SoundBlock
	{
		int buttonSize = block_diameter / 6;
		int buttonDist;
		String beatString = "";

		int clipStartTime = 0;
		int beatCount = 0;
		int beatTimer = 0; // used to time the current beat sound
		int totalLength = millisPerBeat * beatsPerMeasure; // this block will be
															// one measures
		int beatLength;

		boolean pie = true;

		BeatButton[] buttons;
		int numBeats = 16;

		BeatBlock(TuioObject tObj)
		{
			Init(tObj, 0);
		}

		BeatBlock(int x, int y)
		{
			Init(0, x, y, 62);
		}

		public void Setup()
		{
			switch (sym_id)
			{
			case 40:
				numBeats = 8;
				break;
			case 41:
				numBeats = 4;
				break;
			case 42:
				numBeats = 16;
				break;
			}
			canBeChained = false;
			LoadClips();
			buttons = new BeatButton[numBeats];
			for (int i = 0; i < buttons.length; i++)
			{
				buttons[i] = new BeatButton(this, i, 0, 0, 0, buttonSize);
				beatString += "-";
				if (i % 4 == 0)
					buttons[i].Trigger();
			}
			buttonDist = block_diameter / 2 + buttonSize;
			beatLength = millisPerBeat * beatsPerMeasure / numBeats;// millisPerBeat
																	// * 4 / 8,
			arrangeButtons(rotation);

			BeginPlaying();
		}

		public void Update()
		{

			super.Update();
			if (isPlaying)
			{
				beatCount = millis() / (beatLength) % numBeats;
				playTimer = millis() % (totalLength);

				if (pie)
					drawArc((int) (block_diameter * .8f),
							(float) playTimer / (float) totalLength, rotation);
				// else drawBeat((int)(block_diameter * .5));

				if (millis() - clipStartTime >= beatLength)
				{
					if (beatString.charAt(beatCount) == '0'
							|| beatString.charAt(beatCount) == '+')
						PlayBeat();
				}
			}

			drawBridges();
			arrangeButtons(rotation);
		}

		public void UpdatePosition()
		{
			super.UpdatePosition();
			// arrangeButtons(rotation);
		}

		public void Activate(PlayHead play, Block previous)
		{
			super.Activate(play, previous);
			this.previous = previous;
			Play();
		}

		public int[] getSuccessors()
		{
			return new int[] { 0 };
		}

		public void OnRemove()
		{
			super.OnRemove();
		}

		public void Die()
		{
			super.Die();
			for (int i = 0; i < numBeats; i++)
			{
				buttons[i].Destroy();
				buttons[i] = null;
			}
		}

		public boolean isReadyToDie()
		{
			return (true);
		}

		public void arrangeButtons(float startAngle)
		{
			for (int i = 0; i < buttons.length; i++)
			{
				buttons[i].Update(
						(int) (x_pos + cos(startAngle + i * 2 * PI / numBeats)
								* buttonDist),
						(int) (y_pos + sin(startAngle + i * 2 * PI / numBeats)
								* buttonDist),
						i * 2 * PI / numBeats);
			}
		}

		public void drawButtons()
		{
			for (BeatButton butt : buttons)
			{
				if (butt.isShowing)
					butt.drawButton();
			}
		}

		public void drawBridge(int afterIndex)
		{
			noFill();
			strokeWeight(buttonSize);
			// strokeCap(ROUND);
			stroke(invertColor ? 255 : 0);
			float startRot = rotation + afterIndex * 2 * PI / numBeats;
			arc(x_pos, y_pos, buttonDist * 2, buttonDist * 2, startRot,
					startRot + 2 * PI / numBeats);
		}

		public void drawBridges()
		{
			for (int i = 1; i < numBeats; i++)
			{
				if (beatString.charAt(i) == '+')
					drawBridge(i - 1);
			}
		}

		public void drawShadow()
		{
			shapeMode(CORNER);

			beatShadow.setFill(blockColor);
			pushMatrix();
			translate(x_pos, y_pos);
			rotate(rotation);
			shape(beatShadow);
			popMatrix();
		}

		public void draw()
		{
			drawShadow();
			drawButtons();
		}

		public void PlayBeat()
		{
			clips[activeClip].cue(millis() % (beatLength));
			clips[activeClip].play();
			clipStartTime = millis() - (millis() % (beatLength));
		}

		public void BeginPlaying()
		{
			beatCount = 0;
			isPlaying = true;
			PlayBeat();
		}

		public void flipBeat(int i)
		{
			buttons[i].isOn = !buttons[i].isOn;
			char cur = buttons[i].isOn ? '0' : '-';
			setBeat(i, cur);
			if (cur == '-' && beatString.length() > i + 1
					&& beatString.charAt(i + 1) == '+')
				setBeat(i + 1, '0');
		}

		public void setBeat(int i, char beat)
		{
			beatString = beatString.substring(0, i) + beat
					+ beatString.substring(i + 1);
		}
	}

	public class BeatButton extends Button
	{
		BeatBlock block;
		int index;
		boolean isOn;

		BeatButton(BeatBlock b, int i, int x_pos, int y_pos, float rot,
				float rad)
		{
			InitButton(x_pos, y_pos, rot, rad);
			index = i;
			block = b;
			isOn = false;
		}

		public void Trigger(Cursor cursor)
		{
			// the first time a cursor touches a beatbutton, it should record
			// the new state of that button and set all others it touches to
			// that
			block.flipBeat(index);
			if (cursor.beatHistory.size() > 0)
			{
				BeatButton last = cursor.beatHistory
						.get(cursor.beatHistory.size() - 1);
				if (last.block == this.block)
				{
					if (last.index == this.index - 1)
					{
						if (last.isOn && this.isOn)
						{
							block.setBeat(this.index, '+');
						}
					}
					else if (last.index == this.index + 1)
					{
						if (last.isOn && this.isOn)
						{
							block.setBeat(last.index, '+');
						}
					}
				}
			}
		}

		public void Trigger()
		{
			block.flipBeat(index);
		}

		public void drawButton()
		{
			strokeWeight(3);

			if (isOn)
			{
				fill(color(invertColor ? 255 : 0));
				stroke(color(invertColor ? 0 : 255));
			}
			else
			{
				fill(color(invertColor ? 0 : 255));
				stroke(color(invertColor ? 255 : 0));
			}
			ellipse(x, y, size * 2, size * 2);
		}
	}

	public abstract class Block
	{

		TuioObject tuioObj;
		public int sym_id;
		public float x_pos;
		public float y_pos;
		public float rotation;
		public BlockType type;
		int numLeads = 0;
		boolean leadsActive = false;
		boolean inChain = false;
		boolean canBeChained = true;
		boolean isMissing = false;
		boolean isFake;
		int blockColor;

		float block_width;

		public ArrayList<Block> parents;
		public Block[] children;
		public Lead[] leads;

		LinkedList<PlayHead> playHeadList = new LinkedList<PlayHead>(); // currently
																		// unused
		PlayHead playHead; // when a playHead passes through this block, it is
							// kept track of here

		ArrayList<PVector> posHistory = new ArrayList<PVector>(); // used to
																	// calculate
																	// average
																	// position
		ArrayList<Float> rotHistory = new ArrayList<Float>(); // used to
																// calculate
																// average
																// rotation
		int historyVals = 10; // how many position and rotation values to store
								// for calculating a moving average (data
								// smoothing)
		int missingSince = 0; // millis when block is reported removed by TUIO
		final int deathDelay = 500; // how many millis after going missing for
									// the block to be removed

		public abstract void Setup();

		public abstract int[] getSuccessors();

		public void Init(TuioObject tobj, int numLeads)
		{
			this.numLeads = numLeads;
			parents = new ArrayList<Block>();
			children = new Block[numLeads];
			leads = new Lead[numLeads];

			setTuioObject(tobj);
			allBlocks.add(this);

			for (int i = 0; i < numLeads; i++)
			{
				leads[i] = new Lead(this, rotation + i * 2 * PI / numLeads, i);
			}
			UpdatePosition();
			arrangeLeads(rotation);
			type = idToType.get(sym_id);

			blockColor = color(invertColor ? 255 : 0);
			Setup();
		}

		/*
		 * This Init is for simulated blocks and does not require a TuioObject
		 */
		public void Init(int numLeads, int x, int y, int id)
		{
			this.numLeads = numLeads;
			this.sym_id = id;
			allBlocks.add(this);

			parents = new ArrayList<Block>();
			children = new Block[numLeads];
			leads = new Lead[numLeads];

			for (int i = 0; i < numLeads; i++)
			{
				leads[i] = new Lead(this, rotation + i * 2 * PI / numLeads, i);
			}
			type = idToType.get(sym_id);

			blockColor = color(invertColor ? 255 : 0);
			isFake = true;
			x_pos = x;
			y_pos = y;

			Setup();
		}

		public void setTuioObject(TuioObject tobj)
		{
			tuioObj = tobj;
			sym_id = tobj.getSymbolID();
			blockMap.put(tobj.getSessionID(), this);
		}

		public void Update()
		{
			// Update leads and break connection if the child block is too far
			// away
			for (int i = 0; i < numLeads; i++)
			{
				Lead l = leads[i];
				l.Update();
				if (l.occupant != null && l.occupantTooFar())
				{
					breakConnection(i, true);
				}
			}

			// checks to see if this block has been missing for too long
			if (isMissing)
			{
				if (millis() - missingSince >= deathDelay)
				{
					if (isReadyToDie())
					{
						missingBlocks.remove(this);
						killBlocks.add(this);
					}
				}
			}

			// fake blocks never use UpdatePosition(), which is where
			// updateNeighbors is normally called
			if (isFake)
			{
				updateNeighbors();
				/*
				 * for(Lead l : leads){ l.UpdateRotationFromParent(.01); }
				 */
			}
		}

		/*
		 * Called when the TuioObject is reported removed. It won't actually be
		 * destroyed until it has been missing for deathDelay milliseconds
		 */
		public void OnRemove()
		{
			missingBlocks.add(this);
			isMissing = true;
			missingSince = millis();
			if (!isFake)
				blockMap.remove(tuioObj.getSessionID());
		}

		/*
		 * Called when this block has been found while missing
		 */
		public void find(TuioObject newObj)
		{
			isMissing = false;
			missingBlocks.remove(this);
			if (!isFake)
				setTuioObject(newObj);
		}

		/*
		 * Destroys the block and all of its connections
		 */
		public void Die()
		{
			breakAllConnections();
			allBlocks.remove(this);
			missingBlocks.remove(this);
			if (!isFake)
				blockMap.remove(tuioObj.getSessionID());
		}

		// Some blocks may have certain conditions to meet before they're ready
		// to die
		public boolean isReadyToDie()
		{
			return true;
		}

		// previous is the block that has directed the PlayHead to this block
		public void Activate(PlayHead play, Block previous)
		{
			playHead = play;
		}

		/*
		 * Called when this block has finished being active This tells the
		 * playhead to move on to the next block in the chain
		 */
		public void finish()
		{
			// println("finish " + this + " playHead " + (playHead == null ?
			// "null" : playHead.toString()));
			PlayHead temp = playHead;
			if (playHead != null)
			{
				playHead = null;
				// temp.playColor = this.blocolor;
				temp.travel();
			}
		}

		/*
		 * Uses the TuioObject to update the blocks position and rotation
		 * Position and rotation are calculated using a moving average, which
		 * prevents them from jittering as much
		 */
		public void UpdatePosition()
		{
			float new_x_pos = tuioObj.getScreenX(width);
			float new_y_pos = tuioObj.getScreenY(height);

			rotHistory.add(tuioObj.getAngle());
			posHistory.add(new PVector(new_x_pos, new_y_pos));

			if (posHistory.size() < historyVals)
			{
				x_pos = new_x_pos;
				y_pos = new_y_pos;
				rotation = tuioObj.getAngle();
			}
			else
			{

				if (posHistory.size() > historyVals)
				{
					posHistory.remove(0);
				}

				if (rotHistory.size() > historyVals)
				{
					rotHistory.remove(0);
				}

				float avg_x = 0;
				float avg_y = 0;

				float avg_rot = 0;
				float sum_cos = 0;
				float sum_sin = 0;

				for (int i = 0; i < posHistory.size(); i++)
				{
					avg_x += posHistory.get(i).x;
					avg_y += posHistory.get(i).y;
				}

				for (int i = 0; i < rotHistory.size(); i++)
				{
					sum_cos += cos(rotHistory.get(i));
					sum_sin += sin(rotHistory.get(i));
				}

				avg_rot = atan2(sum_sin, sum_cos);

				avg_x = avg_x / posHistory.size();
				avg_y = avg_y / posHistory.size();

				x_pos = avg_x;
				y_pos = avg_y;

				float rotDelta = (avg_rot - rotation); // this will cause
														// unconnected leads to
														// rotate with the block

				for (Lead l : leads)
				{
					l.UpdateRotationFromParent(rotDelta);
				}
				rotation = avg_rot;
			}

			updateNeighbors();
		}

		/*
		 * Returns whether or not the child at index i will be an active
		 * successor to this block
		 */
		public boolean childIsSuccessor(int i)
		{
			return (i < numLeads);
		}

		public void updateNeighbors()
		{

			if (this.canBeChained && !this.inChain)
			{
				findParents();
			}
			if (leadsActive)
			{
				findChildren();
			}
		}

		public void findChildren()
		{
			for (int i = 0; i < numLeads; i++)
			{
				if (leads[i].canRecieveChild())
				{
					for (Block block : allBlocks)
					{
						if (!(block == this || block.parents.contains(this)
								|| this.parents.contains(block)
								|| block.inChain)
								&& leads[i].isUnderBlock(block)
								&& block.canBeChained)
						{
							makeConnection(block, i);
							break;
						}
					}
				}
			}
		}

		public void findParents()
		{
			for (Block block : allBlocks)
			{
				for (int i = 0; i < block.numLeads; i++)
				{
					if (block.leads[i].canRecieveChild())
					{
						if (!(block == this || block.parents.contains(this)
								|| this.parents.contains(block))
								&& block.leadsActive
								&& block.leads[i].isUnderBlock(this))
						{
							block.makeConnection(this, i);
						}
					}
				}
			}
		}

		public void breakAllConnections()
		{
			breakParentConnections();
			breakChildConnections();
		}

		public void breakChildConnections()
		{
			println(this + " start breaking child connections");
			for (int i = 0; i < numLeads; i++)
			{
				breakConnection(i, false);
			}
			println(this + " finished breaking child connections");

		}

		public void breakParentConnections()
		{
			println(this + " start breaking parent connections");
			Block[] parentsArray = new Block[parents.size()];
			parents.toArray(parentsArray);
			for (Block p : parentsArray)
			{
				println("Parent " + p + " break connection with " + this);
				p.breakConnection(this, true);
			}
			println(this + " finished breaking barent connections");
		}

		public void makeConnection(Block b, int i)
		{
			leads[i].connect(b);
		}

		public void SetChild(Block b, int i)
		{
			println(this + " Set Child " + b);
			if (children[i] != null)
				RemoveChild(i);
			children[i] = b;
			b.parents.add(this);
		}

		public void RemoveChild(int i)
		{
			println(this + " Remove Child");
			if (children[i] != null)
			{
				children[i].parents.remove(this);
				children[i] = null;
			}
		}

		/*
		 * Breaks connection with a child at index i
		 */
		public void breakConnection(int i, boolean connectAround)
		{
			println(this + " breakConnection(" + i + "," + connectAround + ")");
			leads[i].disconnect(connectAround);
		}

		/*
		 * Breaks connection with child block b
		 */
		public void breakConnection(Block b, boolean connectAround)
		{

			for (int i = 0; i < numLeads; i++)
			{
				if (children[i] == b)
				{
					leads[i].disconnect(connectAround);
				}
			}
		}

		public void arrangeLeads(float parentLeadRot)
		{

			if (parents.size() > 1)
				return;
			if (leads.length == 0)
				return;
			float leadSeparation = 2 * PI / leads.length;
			float startAngle = PI + parentLeadRot + leadSeparation / 2;

			for (int i = 0; i < leads.length; i++)
			{
				float leadAngle = (startAngle + leadSeparation * i) % (2 * PI);
				leads[i].SetRotation(leadAngle);
			}
		}

		public void cleanLeads()
		{
			for (int i = 0; i < leads.length; i++)
			{
				for (int j = 0; j < leads[i].lines.length; j++)
				{
					leads[i].lines[j].visible = false;
				}
			}
		}

		public void draw()
		{
			drawShadow();
		}

		/*
		 * Default shadow shape is a circle. We use shapes instead of ellipse()
		 * to improve performance
		 */
		public void drawShadow()
		{
			noStroke();
			shapeMode(CENTER);
			fill(blockColor);
			pushMatrix();
			translate(x_pos, y_pos);
			shape(circleShadow);
			popMatrix();
		}

		/*
		 * Updates the look of this block's lead depending on whether or not
		 * it's in an active path from a Start block
		 */
		public void updateLeads(float offset, int col, boolean isActive,
				boolean setBlockColor, ArrayList<Block> activeVisited,
				ArrayList<Block> inactiveVisited)
		{
			Block origin = activeVisited.get(0);
			int originId = origin.sym_id;
			this.inChain = true;
			colorMode(HSB);

			int dulledColor = color(hue(col), saturation(col) / 3, 150);
			if (setBlockColor)
				blockColor = (isActive ? col : dulledColor);

			for (int i = 0; i < numLeads; i++)
			{
				if (isActive && childIsSuccessor(i))
				{
					leads[i].lines[originId].dashed = true;
					leads[i].lines[originId].dash_offset = offset;
					leads[i].lines[originId].col = col;
					leads[i].lines[originId].visible = true;

					if (children[i] != null
							&& !activeVisited.contains(children[i]))
					{
						activeVisited.add(children[i]);
						children[i].updateLeads(offset, col, true,
								setBlockColor, activeVisited, inactiveVisited);
					}
				}
				else
				{
					leads[i].lines[originId].dashed = false;
					leads[i].lines[originId].col = dulledColor;
					leads[i].lines[originId].visible = true;

					if (children[i] != null
							&& !activeVisited.contains(children[i])
							&& !inactiveVisited.contains(children[i]))
					{
						inactiveVisited.add(children[i]);
						children[i].updateLeads(offset, col, false,
								setBlockColor, activeVisited, inactiveVisited);
					}
				}
			}
		}

		/*
		 * Draws an arc around the block's center with specified radius,
		 * starting rotation and completion percentage of the arc.
		 */
		public void drawArc(int radius, float percent, float startRotation)
		{
			pushMatrix();
			noStroke();

			if (playHead != null)
				fill(playHead.playColor);
			else
				fill(blockColor);
			translate(x_pos, y_pos);
			rotate(startRotation);
			arc(0, 0, block_diameter + radius, block_diameter + radius, 0,
					percent * 2 * PI, // (float)clip.position()/(float)clip.length()
										// * 2*PI,
					PIE);
			popMatrix();
		}

		/*
		 * Draws a circle under the block with the max specified radius at the
		 * start of each beat. The circle shrinks over time until the start of
		 * the next beat
		 */
		public void drawBeat(int radius)
		{
			pushMatrix();
			fill(blockColor);
			noStroke();
			translate(x_pos, y_pos);
			rotate(0); // should rotate such that the start angle points to the
						// parent
			ellipse(0, 0, radius, radius);
			popMatrix();
		}

		public void drawLeads()
		{

			for (Lead l : leads)
			{
				l.draw();
			}
		}

		public boolean IsUnder(int hit_x, int hit_y)
		{
			return (dist(hit_x, hit_y, x_pos, y_pos) < block_diameter / 2);
		}

		public String toString()
		{
			return ("id: " + sym_id + "  x: " + x_pos + "  y: " + y_pos);
		}
	}

	public class BooleanBlock extends Block
	{

		BooleanBlock(TuioObject tObj)
		{
			Init(tObj, 0);
		}

		BooleanBlock(int x, int y)
		{
			Init(0, x, y, 100);
		}

		public void Setup()
		{
			boolMap.put(sym_id, this);
			canBeChained = false;
		}

		public void Update()
		{
			super.Update();
		}

		public void OnRemove()
		{
			super.OnRemove();
		}

		public void Die()
		{
			super.Die();
			boolMap.remove(sym_id);

		}

		public void drawShadow()
		{
			stroke(blockColor);
			strokeWeight(20);
			rectMode(CENTER); // Set ellipseMode to CENTER
			fill(blockColor); // Set fill to black
			pushMatrix();
			translate(x_pos, y_pos);

			rotate(rotation);
			rect(0, 0, block_diameter, block_diameter);
			popMatrix();
		}

		public void Activate(PlayHead play, Block previous)
		{
			super.Activate(play, previous);
		}

		public int[] getSuccessors()
		{
			return new int[] {};
		}

	}

	public abstract class Button
	{
		int x, y;
		float rotation;
		float size;
		public boolean isShowing;

		public void InitButton(int x_pos, int y_pos, float rot, float rad)
		{
			isShowing = true;
			x = x_pos;
			y = y_pos;
			rotation = rot;
			size = rad;
			allButtons.add(this);
		}

		public void Update(int x_pos, int y_pos, float rot)
		{
			x = x_pos;
			y = y_pos;
			rotation = rot;
		}

		public void Destroy()
		{
			allButtons.remove(this);
		}

		public boolean IsUnder(int hit_x, int hit_y)
		{
			return (dist(hit_x, hit_y, x, y) < size);
		}

		public void FlipShowing()
		{
			isShowing = !isShowing;
		}

		public void SetShowing(boolean b)
		{
			isShowing = b;
		}

		public abstract void Trigger(Cursor cursor);

		public abstract void drawButton();
	}

	public class PlayAllButton extends Button
	{

		PlayAllButton(int x_pos, int y_pos, float rot, float rad)
		{
			InitButton(x_pos, y_pos, rot, rad);
		}

		public void Trigger(Cursor cursor)
		{
			Play();
		}

		public void drawButton()
		{
			fill(color(0, 0, 0));
			ellipse(x, y, size * 2, size * 2);
			pushMatrix();
			translate(x, y);
			scale(.8f);
			translate(size / 6, 0);
			fill(color(255, 255, 255));
			triangle(-size / 2, -size / 2, -size / 2, size / 2, size / 2, 0);
			popMatrix();
		}
	}

	public class PlusButton extends Button
	{
		StartLoopBlock block;

		PlusButton(int x_pos, int y_pos, float rot, float rad, StartLoopBlock b)
		{
			InitButton(x_pos, y_pos, rot, rad);
			block = b;
		}

		public void Trigger(Cursor cursor)
		{

			block.IncrementCount(true);
		}

		public void drawButton()
		{
			if (!block.inChain)
				return;

			pushMatrix();
			translate(x, y);
			// translate(size/6, 0);
			rotate(rotation);

			fill(invertColor ? 255 : 0);
			noStroke();
			ellipse(0, 0, size * 2, size * 2);

			fill(invertColor ? 0 : 255);
			textAlign(CENTER, CENTER);
			textSize(size * 2);
			text("+", 0, -size / 5);

			popMatrix();
		}
	}

	public class MinusButton extends Button
	{
		StartLoopBlock block;

		MinusButton(int x_pos, int y_pos, float rot, float rad,
				StartLoopBlock b)
		{
			InitButton(x_pos, y_pos, rot, rad);
			block = b;
		}

		public void Trigger(Cursor cursor)
		{
			block.DecrementCount(true);
		}

		public void drawButton()
		{
			if (!block.inChain)
				return;
			pushMatrix();
			translate(x, y);
			// translate(size/6, 0);
			rotate(rotation);

			fill(invertColor ? 255 : 0);
			noStroke();
			ellipse(0, 0, size * 2, size * 2);

			fill(invertColor ? 0 : 255);
			textAlign(CENTER, CENTER);
			textSize(size * 2);
			text("-", 0, -size / 3);

			popMatrix();

		}
	}

	public class CallBlock extends Block
	{
		int functionId;
		FunctionBlock function;

		EndLead endLead;

		boolean returning = false;

		// ArrayList<PlayHead>

		CallBlock(TuioObject tObj)
		{
			Init(tObj, 2);
		}

		CallBlock(int x, int y)
		{
			Init(2, x, y, 122);
		}

		public void Setup()
		{
			endLead = new EndLead(this);
			// functionId = sym_id - 5; this is how it should be, but we messed
			// up the icons
			// this switch statement is a quick fix
			functionId = 0;
			switch (sym_id)
			{
			case 8:
				functionId = 3;
				break;
			case 7:
				functionId = 2;
				break;
			case 5:
				functionId = 1;
				break;
			}

			leads[1].options.unlimitedDistance = true;
			leads[1].options.visible = false;
			endLead.options.visible = false;

			CheckForFunction();
		}

		public void Update()
		{
			super.Update();
			leadsActive = inChain;

			CheckForFunction();
			if (function != null)
			{
				endLead.SetTargetLead(lastActiveLeadInChain(function));
			}
			endLead.Update();
		}

		public void OnRemove()
		{
			super.OnRemove();
		}

		public void Activate(PlayHead play, Block previous)
		{
			// if the previous block is not a parent, we can assume it is in
			// another chain
			if (!parents.contains(previous))
			{
				returning = true;
			}
			else
			{
				play.addFunctionCall(this);
			}
			super.Activate(play, previous);
			finish();
			returning = false;
		}

		public void CheckForFunction()
		{
			if (function != null && !funcMap.containsKey(functionId))
			{
				function = null;
				leads[1].disconnect(false);
			}
			else if (function == null && funcMap.containsKey(functionId))
			{
				function = funcMap.get(functionId);
				leads[1].connect(function);
			}
		}

		public void arrangeLeads(float parentLeadRot)
		{

			if (parents.size() > 1)
				return;
			if (leads.length == 1)
				return;
			float leadSeparation = 2 * PI / (leads.length - 1);
			float startAngle = PI + parentLeadRot + leadSeparation / 2;

			for (int i = 0; i < leads.length - 1; i++)
			{
				float leadAngle = (startAngle + leadSeparation * i) % (2 * PI);
				leads[i].SetRotation(leadAngle);
			}
		}

		public void drawLeads()
		{

			for (Lead l : leads)
			{
				l.draw();
			}

			endLead.draw();
		}

		public int[] getSuccessors()
		{
			if (function == null || returning)
			{
				return new int[] { 0 };
			}
			else
			{
				return new int[] { 1 };
			}
		}

		public void updateLeads(float offset, int col, boolean isActive,
				boolean setBlockColor, ArrayList<Block> activeVisited,
				ArrayList<Block> inactiveVisited)
		{
			super.updateLeads(offset, col, isActive, setBlockColor,
					activeVisited, inactiveVisited);
			endLead.lines = leads[1].lines;
		}
	}

	public class ClipBlock extends SoundBlock
	{
		int buttonSize = block_diameter / 4;
		int buttonDist = block_diameter / 2;
		ClipButton[] buttons;

		ClipBlock(TuioObject tObj)
		{
			Init(tObj, 1);
		}

		ClipBlock(int x, int y, int id)
		{
			Init(1, x, y, id);
		}

		public void Setup()
		{
			LoadClips();
			buttons = new ClipButton[clips.length];
			for (int i = 0; i < buttons.length; i++)
			{
				buttons[i] = new ClipButton(this, i, 0, 0, 0, buttonSize);
			}
			arrangeButtons(rotation);
		}

		public void Update()
		{
			super.Update();
			leadsActive = inChain;

			if (isPlaying)
			{

				if (pie)
				{
					float arcRot = 0;
					if (previous != null)
					{
						arcRot = atan2((previous.y_pos - this.y_pos),
								(previous.x_pos - this.x_pos));
					}
					drawArc((int) (block_diameter * .4f),
							(float) clips[activeClip].position()
									/ (float) clips[activeClip].length(),
							arcRot);
				}
				else
				{
					float beatRadius = block_diameter * 1.25f * (1.0f
							- .5f * (float) (clips[activeClip].position() % 255)
									/ (float) 255);

					drawBeat((int) (beatRadius));
				}
				playTimer += millis() - startTime;
				if (clips[activeClip].position() >= clips[activeClip].length())
				{
					Stop();
					finish();
				}
			}
			arrangeButtons(rotation);

		}

		public void Activate(PlayHead play, Block previous)
		{
			super.Activate(play, previous);

		}

		public int[] getSuccessors()
		{
			return new int[] { 0 };
		}

		public void OnRemove()
		{
			super.OnRemove();
		}

		public void arrangeButtons(float startAngle)
		{
			for (int i = 0; i < buttons.length; i++)
			{
				buttons[i].Update(
						(int) (x_pos
								+ cos(startAngle + i * 2 * PI / clips.length)
										* buttonDist),
						(int) (y_pos
								+ sin(startAngle + i * 2 * PI / clips.length)
										* buttonDist),
						startAngle + i * 2.0f * PI / clips.length);
			}
		}

		public void SetButtonsShowing(boolean isShowing)
		{
			for (ClipButton butt : buttons)
			{
				butt.SetShowing(isShowing);
			}
		}

		public void drawButtons()
		{
			for (ClipButton butt : buttons)
			{
				if (butt.isShowing)
					butt.drawButton();
			}
		}

		public void draw()
		{
			drawButtons();
			drawShadow();
		}

		public void Die()
		{
			super.Die();
			for (int i = 0; i < buttons.length; i++)
			{
				buttons[i].Destroy();
				buttons[i] = null;
			}
			if (playHead != null)
			{
				playHead.dead = true;
			}
		}
	}

	public class ClipButton extends Button
	{
		ClipBlock block;
		int index;

		ClipButton(ClipBlock b, int i, int x_pos, int y_pos, float rot,
				float rad)
		{
			InitButton(x_pos, y_pos, rot, rad);
			index = i;
			block = b;
		}

		public void Trigger(Cursor cursor)
		{
			block.SwitchClip(index);
		}

		public void Trigger()
		{
			block.SwitchClip(index);
		}

		public void drawButton()
		{
			strokeWeight(3);
			strokeCap(SQUARE);
			int strokeCol = (color(invertColor ? 255 : 0));
			int fillCol = (color(invertColor ? 0 : 255));

			if (block.activeClip == index)
			{
				fillCol = (color(invertColor ? 255 : 0));
				strokeCol = (color(invertColor ? 0 : 255));
			}

			fill(fillCol);
			stroke(strokeCol);

			pushMatrix();
			translate(x, y);
			rotate(rotation);

			rotate(HALF_PI);
			// arc(0,0,block_diameter, block_diameter, -QUARTER_PI,QUARTER_PI);
			ellipse(0, 0, size * 2, size * 2);

			textAlign(CENTER, CENTER);
			translate(0, -size / 1.5f);
			textSize(text_size / 2);
			fill(strokeCol);
			text(index, 0, 0);
			popMatrix();

			pushMatrix();
			noFill();
			stroke(strokeCol);
			translate(block.x_pos, block.y_pos);
			// arc(0,0,block_diameter+10, block_diameter +10, rotation - PI/7,
			// rotation + PI/7);
			popMatrix();
		}
	}

	public class ConditionalBlock extends Block
	{
		boolean isTrue = false;
		int boolId;
		int myColor;

		BooleanBlock boolBlock;
		// Lead boolLead;

		BooleanButton leftButton;
		BooleanButton rightButton;

		ConditionalBlock(TuioObject tObj)
		{
			Init(tObj, 2);
		}

		ConditionalBlock(int x, int y)
		{
			Init(2, x, y, 110);
		}

		public void Setup()
		{

			/*
			 * boolId = sym_id - 10; //booleans are 100-109, corresponding
			 * conditionals are 110-119
			 * 
			 * 
			 * 
			 * boolLead = new Lead(this, 0); boolLead.break_distance = 9999;
			 * boolLead.visible = false; boolLead.lines[0].visible = false;
			 * boolLead.lines[0].col = myColor; boolLead.lines[0].dashed =
			 * false; boolLead.lines[0].weight = 5;
			 * 
			 * //leads[0].options.image = unlock; leads[0].image = (isTrue ?
			 * unlock : lock);
			 * 
			 * checkBooleanBlock();
			 */
			leftButton = new BooleanButton(0, 0, 0, block_diameter / 5, this);
			rightButton = new BooleanButton(0, 0, 0, block_diameter / 5, this);

			leftButton.Flip();

			arrangeButtons();

		}

		public void Update()
		{
			super.Update();
			leadsActive = inChain;
			arrangeButtons();

			// checkBooleanBlock();
			// boolLead.Update();
			// boolLead.draw();
		}

		public void arrangeButtons()
		{
			float leftLeadRot = leads[0].rotation;
			float rightLeadRot = leads[1].rotation;

			float leftButtonDist = leads[0].distance / 2; // how far along the
															// lead
			float rightButtonDist = leads[1].distance / 2;

			PVector leftPos = new PVector(
					x_pos + cos(leftLeadRot) * leftButtonDist,
					y_pos + sin(leftLeadRot) * leftButtonDist);

			PVector rightPos = new PVector(
					x_pos + cos(rightLeadRot) * rightButtonDist,
					y_pos + sin(rightLeadRot) * rightButtonDist);

			leftButton.Update((int) (leftPos.x), (int) (leftPos.y),
					leftLeadRot + PI / 2);

			rightButton.Update((int) (rightPos.x), (int) (rightPos.y),
					rightLeadRot + PI / 2);
		}

		/*
		 * void checkBooleanBlock() { boolean inMap =
		 * boolMap.containsKey(boolId); if (inMap && !isTrue) {//the
		 * booleanBlock has just been added to the dictionary isTrue = true;
		 * boolBlock = boolMap.get(boolId); boolLead.connect(boolBlock);
		 * boolLead.lines[0].visible = true; boolLead.lines[0].col =
		 * (invertColor? 255:0); //leads[0].options.image = lock;
		 * 
		 * } else if (!inMap && isTrue) { //the booleanBlock has just been
		 * removed from the dictionary isTrue = false; boolBlock = null;
		 * 
		 * boolLead.disconnect(false); boolLead.lines[0].visible = false;
		 * boolLead.lines[0].col = (invertColor? 0:255);
		 * //leads[0].options.image = unlock; }
		 * 
		 * leads[0].image = (isTrue? unlock : lock); }
		 */

		public void Flip()
		{
			isTrue = !isTrue;
			leftButton.Flip();
			rightButton.Flip();

		}

		public void OnRemove()
		{
			super.OnRemove();
		}

		public void Die()
		{
			leftButton.Destroy();
			leftButton = null;
			rightButton.Destroy();
			rightButton = null;

			super.Die();
		}

		public void drawButtons()
		{
			if (leftButton.isShowing)
			{
				leftButton.drawButton();
			}
			if (rightButton.isShowing)
			{
				rightButton.drawButton();
			}
		}

		public void draw()
		{
			drawShadow();
			drawButtons();
		}

		public void Activate(PlayHead play, Block previous)
		{
			super.Activate(play, previous);
			finish();
		}

		public boolean childIsSuccessor(int i)
		{
			if (i == 0)
				return isTrue;
			else
				return !isTrue;
		}

		public int[] getSuccessors()
		{
			if (isTrue)
				return new int[] { 0 };
			else
				return new int[] { 1 };
		}
	}

	public class BooleanButton extends Button
	{
		ConditionalBlock block;
		boolean isOn;

		BooleanButton(int x_pos, int y_pos, float rot, float rad,
				ConditionalBlock b)
		{
			InitButton(x_pos, y_pos, rot, rad);
			block = b;
		}

		public void Trigger(Cursor cursor)
		{
			block.Flip();
		}

		public void Flip()
		{
			isOn = !isOn;
		}

		public void drawButton()
		{
			if (!block.inChain)
				return;
			pushMatrix();
			translate(x, y);
			// translate(size/6, 0);
			rotate(rotation);

			fill(isOn ? 0 : 255);
			stroke(isOn ? 255 : 0);
			strokeWeight(size / 5);
			ellipse(0, 0, size * 2, size * 2);

			/*
			 * fill(invertColor? 0 : 255); textAlign(CENTER, CENTER);
			 * textSize(size*2); text("-", 0, -size/3);
			 */
			popMatrix();

		}
	}

	public class Cursor
	{
		int x_pos, y_pos;
		TuioCursor tuioCursor;
		int spawnTime;
		int spawnDelay;
		boolean dead;
		boolean isMouse = false;
		ArrayList<BeatButton> beatHistory;

		Cursor(TuioCursor c)
		{
			Init();
			setTuioCursor(c);
		}

		Cursor()
		{
			Init();
			isMouse = true;
		}

		public void Init()
		{
			beatHistory = new ArrayList<BeatButton>();
			spawnTime = millis();
			checkButtons();
			dead = false;
			cursors.add(this);
		}

		public void UpdatePosition()
		{
			x_pos = tuioCursor.getScreenX(width);
			y_pos = tuioCursor.getScreenY(height);
		}

		public void checkButtons()
		{// NullPointerException

			for (Button b : allButtons)
			{
				if (isMouse)
					println("mouse CheckButtons " + b.IsUnder(x_pos, y_pos));
				if (!dead && b.isShowing && b.IsUnder(x_pos, y_pos)
						&& !beatHistory.contains(b))
				{

					b.Trigger(this);
					if (b instanceof BeatButton)
					{
						beatHistory.add((BeatButton) b);
					}
					else
						dead = true;
				}
			}
		}

		public void Update()
		{
			if (isMouse)
			{
				x_pos = mouseX;
				y_pos = mouseY;
			}
			if (!dead)
				checkButtons();

			if (debug)
			{

				stroke(color(255, 0, 0));
				strokeWeight(3);
				noFill();
				ellipse(x_pos, y_pos, 5, 5);
			}
		}

		public void OnRemove()
		{
			cursors.remove(this);

			if (!isMouse)
				cursorMap.remove(tuioCursor.getSessionID());
		}

		public void setTuioCursor(TuioCursor tCur)
		{
			tuioCursor = tCur;
			cursorMap.put(tCur.getSessionID(), this);
		}
	}

	public class EffectBlock extends Block
	{

		EffectBlock(TuioObject tObj)
		{
			Init(tObj, 1);
		}

		public void Setup()
		{
		}

		public void Update()
		{
			super.Update();
		}

		public void OnRemove()
		{
			super.OnRemove();

		}

		public void Activate(PlayHead play, Block previous)
		{
			super.Activate(play, previous);
			finish();
		}

		public int[] getSuccessors()
		{
			return new int[] { 0 };
		}
	}
	/*
	 * EndLeads are only for show. They point from a lead footprint to a block.
	 * Regular leads are only block to block. The owner is the
	 */

	public class EndLead extends Lead
	{
		Lead targetLead;

		EndLead(Block owner)
		{
			super(owner, 0, -1);
			options.reverse_march = true;
		}

		public void SetTargetLead(Lead t)
		{
			targetLead = t;
		}

		public void Update()
		{
			if (targetLead != null)
			{
				trackLead(targetLead);
			}
		}

		public void trackLead(Lead lead)
		{
			PVector pos = lead.footprintPosition();
			rotation = atan2((pos.y - owner.y_pos), (pos.x - owner.x_pos));
			distance = dist(pos.x, pos.y, owner.x_pos, owner.y_pos);
		}

		public boolean showFootprint()
		{
			return false;
		}
	}

	public class FunctionBlock extends Block
	{
		float dashedLineOffset = 0;
		ArrayList<PlayHead> spawnedPlayHeads;
		ExecuteButton executeButt;
		StopButton stopButt;
		boolean waitingForBeat = false;
		int waitUntil;

		FunctionBlock(TuioObject tObj)
		{
			canBeChained = false;

			Init(tObj, 1);

		}

		FunctionBlock(int x, int y, int id)
		{
			canBeChained = false;

			Init(1, x, y, id);

		}

		public void Setup()
		{ // ARRAY INDEX OUT OF BOUNDS
			allFunctionBlocks.add(this);
			funcMap.put(sym_id, this);
			spawnedPlayHeads = new ArrayList<PlayHead>();
			canBeChained = false;

			leadsActive = true;
			executeButt = new ExecuteButton(this, -block_diameter, 0, 0,
					block_diameter / 4);
			stopButt = new StopButton(this, -block_diameter, 0, 0,
					block_diameter / 4);
			stopButt.SetShowing(false);
			blockColor = colorSet[sym_id];
		}

		public void Update()
		{
			super.Update();
			dashedLineOffset = (millis() % (millisPerBeat * .5f)
					/ (float) (millisPerBeat * .5f));

			arrangeButtons();

			if (waitingForBeat || spawnedPlayHeads.size() > 0)
			{
				float beatRadius = block_diameter * 1.5f
						* (1.0f - ((float) (millis() % millisPerBeat))
								/ (float) millisPerBeat);

				drawBeat((int) beatRadius);
			}
			if (waitingForBeat && millis() >= waitUntil)
			{
				createPlayHead();
				waitingForBeat = false;
			}
		}

		public void startUpdatePath()
		{
			ArrayList<Block> activeVisited = new ArrayList<Block>();
			ArrayList<Block> inactiveVisited = new ArrayList<Block>();
			activeVisited.add(this);
			super.updateLeads(dashedLineOffset, blockColor, true, true,
					activeVisited, inactiveVisited);
		}

		public void OnRemove()
		{
			super.OnRemove();
		}

		public void Die()
		{
			super.Die();
			Stop();

			allFunctionBlocks.remove(this);
			funcMap.remove(sym_id);
			executeButt.Destroy();
			executeButt = null;
			stopButt.Destroy();
			stopButt = null;
		}

		public void Activate(PlayHead play, Block previous)
		{
			super.Activate(play, previous);
			finish();
		}

		public int[] getSuccessors()
		{
			return new int[] { 0 };
		}

		public void drawShadow()
		{
			shapeMode(CORNER);
			fill(blockColor);
			// stroke(blockColor);
			// strokeWeight(10);
			noStroke();
			pushMatrix();
			translate(x_pos, y_pos);
			rotate(rotation + 2 * PI / 12);
			shape(playShadow);
			popMatrix();
		}

		public void drawButtons()
		{
			if (stopButt.isShowing)
				stopButt.drawButton();
			if (executeButt.isShowing)
				executeButt.drawButton();
		}

		public void draw()
		{
			drawButtons();
			drawShadow();
		}

		public void updateLeads(float offset, int col, boolean isActive,
				boolean setBlockColor, ArrayList<Block> activeVisited,
				ArrayList<Block> inactiveVisited)
		{
			super.updateLeads(offset, col, isActive, false, activeVisited,
					inactiveVisited);
		}

		public void arrangeButtons()
		{
			int butt_x = (int) (x_pos - cos(rotation) * block_diameter * .75f);
			int butt_y = (int) (y_pos - sin(rotation) * block_diameter * .75f);

			executeButt.Update(butt_x, butt_y, rotation);
			stopButt.Update(butt_x, butt_y, rotation);

		}

		public void execute()
		{
			waitingForBeat = true;
			waitUntil = millis() + (millisPerBeat * beatsPerMeasure
					- millis() % (millisPerBeat * beatsPerMeasure));
			executeButt.SetShowing(false);
			stopButt.SetShowing(true);

		}

		public void createPlayHead()
		{
			PlayHead pHead = new PlayHead(this, this, blockColor);
		}

		public void removePlayHead(PlayHead pHead)
		{
			spawnedPlayHeads.remove(pHead);
			if (spawnedPlayHeads.size() == 0)
			{
				if (stopButt != null) // When the block is removed, buttons may
										// be killed before playheads are
										// removed through the playhead Kill
										// Queue
					stopButt.SetShowing(false);
				if (executeButt != null)
					executeButt.SetShowing(true);
			}
		}

		public void Stop()
		{
			for (PlayHead head : spawnedPlayHeads)
			{
				killPlayHeads.add(head);
			}
			// spawnedPlayHeads.clear();

			stopButt.SetShowing(false);
			executeButt.SetShowing(true);
		}
	}

	public class ExecuteButton extends Button
	{
		FunctionBlock func;

		ExecuteButton(FunctionBlock funcBlock, int x_pos, int y_pos, float rot,
				float rad)
		{
			InitButton(x_pos, y_pos, rot, rad);
			func = funcBlock;
		}

		public void Trigger(Cursor cursor)
		{
			func.execute();
		}

		public void drawButton()
		{
			noStroke();
			fill(invertColor ? 255 : 0);
			ellipse(x, y, size * 2, size * 2);
			pushMatrix();
			translate(x, y);
			scale(.8f);
			translate(size / 6, 0);
			fill(invertColor ? 0 : 255);
			triangle(-size / 2, -size / 2, -size / 2, size / 2, size / 2, 0);
			popMatrix();
		}
	}

	public class StopButton extends Button
	{
		FunctionBlock func;

		StopButton(FunctionBlock funcBlock, int x_pos, int y_pos, float rot,
				float rad)
		{
			InitButton(x_pos, y_pos, rot, rad);
			func = funcBlock;
		}

		public void Trigger(Cursor cursor)
		{
			func.Stop();
		}

		public void drawButton()
		{
			noStroke();
			fill(invertColor ? 255 : 0);
			ellipse(x, y, size * 2, size * 2);
			pushMatrix();
			translate(x, y);
			fill(invertColor ? 0 : 255);
			rectMode(CENTER);
			rect(0, 0, size, size);
			popMatrix();
		}

	}

	int[] colorSet = new int[] { color(155, 89, 182), // amethyst
			color(231, 76, 60), // alizarin red
			color(46, 204, 113), // emerald green
			color(52, 152, 219), // peter river blue
			color(230, 126, 34), // carrot orange

	};

	HashMap<Long, Block> blockMap;

	public void SetupBlockMap()
	{
		blockMap = new HashMap<Long, Block>();
	}

	HashMap<Long, Cursor> cursorMap;

	public void SetupCursorMap()
	{
		cursorMap = new HashMap<Long, Cursor>();
	}

	HashMap<Integer, FunctionBlock> funcMap;

	public void SetupFuncMap()
	{
		funcMap = new HashMap<Integer, FunctionBlock>();
	}

	public HashMap<Integer, String[]> clipDict;

	//TODO ADD AUDIO CLIP
	public void SetupClipDict()
	{
		clipDict = new HashMap<Integer, String[]>();

		clipDict.put(10,
				new String[] { "eightbit/1_eight-bit Atari Pad-001",
						"eightbit/1_eight-bit Atari SineDot-002",
						"eightbit/1_eight-bit Atari SineDot-003" });

		clipDict.put(11,
				new String[] { "eightbit/2_eight-bit Atari Lead-004",
						"eightbit/2_eight-bit Atari Pad-002",
						"eightbit/2_eight-bit Atari Pad-003" });
		clipDict.put(12,
				new String[] { "eightbit/3_eight-bit Atari Lead-010",
						"eightbit/3_eight-bit Atari Synth-001",
						"eightbit/3_eight-bit Atari Synth-002" });
		clipDict.put(13,
				new String[] { "eightbit/4_eight-bit Atari Bassline-003",
						"eightbit/4_eight-bit Atari Bassline-004",
						"eightbit/4_eight-bit Atari Bassline-005" });
		clipDict.put(14,
				new String[] { "eightbit/5_eight-bit Atari Lead-008",
						"eightbit/5_eight-bit Atari Lead-009",
						"eightbit/5_eight-bit Atari Lead-011" });
		clipDict.put(15,
				new String[] { "eightbit/6_eight-bit Atari Lead-003",
						"eightbit/6_eight-bit Atari Pad-004",
						"eightbit/6_eight-bit Atari Synth-003" });

		clipDict.put(40, new String[] {
				"eightbit/MakeBeat/eight-bit Analog Drum Loop-012", });

		clipDict.put(41,
				new String[] {
						// "eightbit/MakeBeat/eight-bit Atari SFX-010",
						"kick_export", });

		clipDict.put(42, new String[] {
				"eightbit/MakeBeat/eight-bit Video Game Loop-010", });

	}

	HashMap<Integer, Integer> idToEffect;

	public void SetupIdToEffect()
	{
		idToEffect = new HashMap<Integer, Integer>();
	}

	HashMap<Integer, BooleanBlock> boolMap;

	public void SetupBoolMap()
	{
		boolMap = new HashMap<Integer, BooleanBlock>();
		/*
		 * boolMap.put(100, false); boolMap.put(101, false); boolMap.put(102,
		 * false); boolMap.put(103, false); boolMap.put(104, false);
		 * boolMap.put(105, false); boolMap.put(106, false); boolMap.put(107,
		 * false); boolMap.put(108, false); boolMap.put(109, false);
		 */
	}

	HashMap<Integer, BlockType> idToType;

	public void SetupIdToType()
	{
		idToType = new HashMap<Integer, BlockType>();

		// FUNCTION 0-4
		for (int i = 0; i < 5; i++)
		{
			idToType.put(i, BlockType.FUNCTION);
		}

		// CALL 5-9
		for (int i = 5; i < 10; i++)
		{
			idToType.put(i, BlockType.CALL);
		}
		idToType.put(122, BlockType.CALL);

		// CLIPS 10-39
		for (int i = 10; i < 40; i++)
		{
			idToType.put(i, BlockType.CLIP);
		}

		// BEATS 40-45
		for (int i = 40; i < 46; i++)
		{
			idToType.put(i, BlockType.BEAT);
		}

		// CONDITIONALS 50-59
		for (int i = 50; i < 60; i++)
		{
			idToType.put(i, BlockType.CONDITIONAL);
		}
		idToType.put(110, BlockType.CONDITIONAL);

		// SPLIT 60
		idToType.put(60, BlockType.SPLIT);
		idToType.put(120, BlockType.SPLIT);

		// LOOPS 61, 62
		idToType.put(61, BlockType.START_LOOP);

		// BOOLEANS 100-109
		for (int i = 100; i < 110; i++)
		{
			idToType.put(i, BlockType.BOOLEAN);
		}
	}

	public class Lead
	{
		public int leadIndex;
		public Block owner;
		public Block occupant;
		float rotation;
		float distance;
		float reg_distance = block_diameter * 1.5f;
		float break_distance = block_diameter * 2; // at what distance will a
													// connection break;
		float connect_snap_dist = block_diameter / 2; // how close does a block
														// need to be to connect
														// to this lead?
		public boolean occupied;
		int lineSeparation = 1;
		int standardWeight = 10;

		public LineOptions[] lines;
		public LeadOptions options;

		Lead(Block owner, float rot, int index)
		{
			this.owner = owner;
			this.rotation = rot;
			leadIndex = index;
			occupied = false;
			distance = reg_distance;
			options = new LeadOptions();
			lines = new LineOptions[5];
			for (int i = 0; i < lines.length; i++)
			{
				lines[i] = new LineOptions();
				lines[i].visible = false;
				lines[i].weight = standardWeight;
			}
		}

		Lead(Block owner, float rot, int index, LeadOptions options)
		{
			this(owner, rot, index);
			this.options = options;
		}

		public void Update()
		{
			if (occupied)
			{
				trackBlock(occupant);
			}
		}

		public int numLinesVisible()
		{
			int num = 0;
			for (int i = 0; i < lines.length; i++)
			{
				if (lines[i].visible)
					num++;
			}
			return num;
		}

		public void draw()
		{
			if (!options.visible)
				return;
			colorMode(RGB, 255);

			int numVisible = numLinesVisible();
			int numVisibleVisited = 0;
			float start_x = -1 * (standardWeight * numVisible
					+ lineSeparation * (numVisible - 1)) / 2;

			pushMatrix();
			translate(owner.x_pos, owner.y_pos);
			rotate(rotation);

			for (int i = 0; i < lines.length; i++)
			{
				LineOptions lineOptions = lines[i];
				if (!lineOptions.visible)
					continue;
				stroke(lineOptions.col);
				strokeWeight(lineOptions.weight);

				pushMatrix();
				int offset_x = (numVisibleVisited
						* (standardWeight + lineSeparation));
				lines[i].x_offset = (start_x + offset_x);
				translate(0, start_x + offset_x);

				if (lineOptions.dashed)
				{
					dashedLine(0, 0, (int) distance, 0,
							(lineOptions.marching ? (options.reverse_march
									? -lineOptions.dash_offset
									: lineOptions.dash_offset) : 0));
				}
				else
				{
					line(0, 0, distance - block_diameter / 2, 0);
				}

				popMatrix();
				numVisibleVisited++;
			}

			if (showFootprint())
			{
				pushMatrix();
				translate(distance, 0);
				dashCircle.setStroke(color(255));// lines[i].col);
				dashCircle.setStrokeWeight(5);
				shapeMode(CENTER);
				shape(dashCircle);
				popMatrix();
			}

			if (options.image != null)
			{
				pushMatrix();
				translate(distance / 2, 0);
				rotate(PI / 2.0f);
				translate(-options.image.width / 2, -options.image.height / 2);
				fill((invertColor ? 0 : 255));
				noStroke();
				rectMode(CORNER);
				rect(0, 0, options.image.width, options.image.height);

				image(options.image, 0, 0);
				popMatrix();
			}

			if (options.showNumber)
			{

				pushMatrix();
				translate(distance / 2, 0);
				rotate(PI / 2.0f);

				fill((invertColor ? 0 : 255)); // should match background
				stroke(255);
				strokeWeight(text_size / 10);
				ellipseMode(CENTER);
				ellipse(0, text_size / 10, text_size, text_size);

				textAlign(CENTER, CENTER);
				textSize(text_size);
				fill((invertColor ? 255 : 0)); // text color
				text(options.number, 0, 0);

				popMatrix();
			}

			popMatrix();
		}

		public void highlightTravelled(int lineNum, float percent, int col)
		{
			percent = min(1, percent);
			percent = max(0, percent);
			stroke(col);
			strokeWeight(block_diameter / 4);
			strokeCap(SQUARE);

			pushMatrix();

			translate(owner.x_pos, owner.y_pos);
			// translate(lines[lineNum].x_offset, 0);
			rotate(rotation);
			// translate(block_diameter/2, 0);

			if (options.reverse_march)
			{
				line(0, 0, (distance) * (percent), 0);
			}
			else
			{
				line((distance) * (1.0f - percent), 0, distance, 0);
			}

			popMatrix();
		}

		public boolean isUnderBlock(Block b)
		{
			PVector look = footprintPosition();
			return (dist(look.x, look.y, b.x_pos,
					b.y_pos) <= connect_snap_dist);
		}
		
		//TODO MODIFY LEAD EVENT
		public void connect(Block block)
		{
			occupant = block;
			occupied = true;
			trackBlock(block);

			System.out.println("UPDATED LEAD OCCUPIED / CONNECTED: " + occupied + " by / to " + occupant.type + " at (" + occupant.x_pos + ", " + occupant.y_pos + ")");

			if (leadIndex >= 0)
			{
				owner.SetChild(block, leadIndex);
			}
			if (block.parents.size() == 1)
			{
				block.arrangeLeads(rotation);
			}

			System.out.println("Appending at ADD LEAD");
			appendSessionGraph();
		}

		public boolean showFootprint()
		{
			return !occupied;
		}

		//TODO MODIFY LEAD EVENT
		public void disconnect(boolean connectAround)
		{
			//TODO ADD CONNECT AROUND REMOVED BLOCK?
			occupied = false;
			if(occupant != null)
			{
				System.out.println("UPDATED LEAD OCCUPIED / CONNECTED: " + occupied + " by / to " + occupant.type + " at (" + occupant.x_pos + ", " + occupant.y_pos + ")");
			}
			occupant = null;
			distance = reg_distance;
			
			if (leadIndex >= 0)
			{
				owner.RemoveChild(leadIndex);
			}

			System.out.println("Appending at REMOVE LEAD");
			appendSessionGraph();
		}

		public void trackBlock(Block block)
		{
			rotation = atan2((block.y_pos - owner.y_pos),
					(block.x_pos - owner.x_pos));
			distance = dist(block.x_pos, block.y_pos, owner.x_pos, owner.y_pos);
		}

		public void SetRotation(float rot)
		{
			rotation = rot;
		}

		// returns whether or not this leads's occupant is too far away to
		// maintain the connection
		public boolean occupantTooFar()
		{
			return options.unlimitedDistance ? false
					: distance > break_distance;
		}

		public PVector footprintPosition()
		{
			float x = owner.x_pos + cos(rotation) * distance;
			float y = owner.y_pos + sin(rotation) * distance;
			return new PVector(x, y);

		}

		public boolean canRecieveChild()
		{
			return occupant == null;
		}

		
		public void UpdateRotationFromParent(float rotDelta)
		{
			if (!occupied)
			{
				rotation += rotDelta;
			}

		}
	}

	public class LineOptions
	{
		public boolean visible = false;
		public boolean dashed = false;
		public boolean marching = true;
		public float dash_offset = 0;
		public float x_offset = 0;
		public int col = color(invertColor ? 255 : 0);
		public int weight = 10;
	}

	public class LeadOptions
	{
		public boolean reverse_march = false;
		public boolean visible = true;
		public PImage image;
		public int number = 0;
		public boolean showNumber = false;
		public boolean unlimitedDistance = false;
	}

	public class StartLoopBlock extends Block
	{
		int count = 1;
		int max_count = 9;
		int displayCount = 0;

		boolean justDepleted = false;

		PlusButton plus;
		MinusButton minus;

		// ArrayList<LoopLead> loopLeads;
		ArrayList<Block> blocksInLoop;
		PVector loopCenter;
		float loopRadius = block_diameter;

		LoopLead headLoopLead;

		StartLoopBlock(TuioObject tObj)
		{
			Init(tObj, 2);
		}

		StartLoopBlock(int x, int y)
		{
			Init(2, x, y, 121);
		}

		public void Setup()
		{
			plus = new PlusButton(0, 0, 0, block_diameter / 4, this);
			minus = new MinusButton(0, 0, 0, block_diameter / 4, this);

			loopCenter = convertFromPolar(new PVector(x_pos, y_pos), rotation,
					block_diameter);

			blocksInLoop = new ArrayList<Block>();
			// blocksInLoop.add(this);
			headLoopLead = new LoopLead(this, this, this, this, 0);
			leads[0] = headLoopLead;
			updateCountLead();
		}

		public void Update()
		{
			super.Update();
			leadsActive = inChain;
			UpdateLoopCenter();
			UpdateLoopRadius();
			arrangeButtons();

			if (inChain)
			{
				drawNumber();
				plus.SetShowing(true);
				minus.SetShowing(true);
			}
			else
			{
				plus.SetShowing(false);
				minus.SetShowing(false);
			}
		}

		public void OnRemove()
		{
			super.OnRemove();
		}

		public void Activate(PlayHead play, Block previous)
		{
			super.Activate(play, previous);

			if (count > 0)
			{
				// play.addStartLoop(this);
			}
			if (count == 1)
				justDepleted = true;
			else
				justDepleted = false;

			DecrementCount(false);

			finish();
			justDepleted = false;
		}

		public void updateCountLead()
		{
			displayCount = count;
			// TODO choose number to represent infinity, if count is that
			// number, showNumber = false, image = infinity.jpg
		}

		public void UpdateLoopCenter()
		{
			PVector midpoint = new PVector();
			for (Block b : blocksInLoop)
			{
				midpoint.x += b.x_pos;
				midpoint.y += b.y_pos;
			}
			midpoint.x = midpoint.x / blocksInLoop.size();
			midpoint.y = midpoint.y / blocksInLoop.size();

			float rot = atan2((midpoint.y - this.y_pos),
					(midpoint.x - this.x_pos));
			// leads[0].rotation = rot;

			float dist = dist(x_pos, y_pos, midpoint.x, midpoint.y);

			float avg_radius = min(block_diameter * 2, dist);
			// loopRadius = avg_radius;

			// PVector testCenter = convertFromPolar(new PVector(x_pos, y_pos),
			// leads[0].rotation, 120);

			loopCenter = convertFromPolar(new PVector(x_pos, y_pos),
					leads[0].rotation, loopRadius);
			// loopCenter = testCenter;
		}

		// Find the average distance of every block in the loop from the loop
		// center, sets loop radius to that
		public void UpdateLoopRadius()
		{
			if (blocksInLoop.size() == 1)
			{
				loopRadius = block_diameter;
				return;
			}
			float total_dist = 0;
			for (Block block : blocksInLoop)
			{
				total_dist += dist(block.x_pos, block.y_pos, loopCenter.x,
						loopCenter.y);
			}
			loopRadius = min(total_dist / blocksInLoop.size(),
					block_diameter * 2);
		}

		public boolean childIsSuccessor(int i)
		{

			if (justDepleted)
			{
				if (i == 0)
					return true;
				else
					return false;
			}

			if (i == 0)
				return (count > 0);
			else
				return !(count > 0);
		}

		public int[] getSuccessors()
		{

			if (count > 0 || justDepleted)
				return new int[] { 0 };
			else
				return new int[] { 1 };
		}

		public void DecrementCount(boolean cycle)
		{
			count--;
			if (count < 0)
			{
				if (cycle)
				{
					count = max_count;
				}
				else
					count = 0;
			}
			updateCountLead();
		}

		public void IncrementCount(boolean cycle)
		{
			count++;
			if (count > max_count)
			{
				if (cycle)
					count = 0;
				else
					count = max_count;
			}
			updateCountLead();
		}

		public void arrangeButtons()
		{
			float countLeadRot = leads[0].rotation;
			float buttonDist = block_diameter * .75f; // how far along the lead
			float buttonLeadOffset = block_diameter / 2.5f; // how far from the
															// lead

			PVector buttonCenter = new PVector(
					x_pos + cos(countLeadRot) * buttonDist,
					y_pos + sin(countLeadRot) * buttonDist);
			PVector plusPos = new PVector(
					buttonCenter.x
							+ cos(countLeadRot - PI / 2) * buttonLeadOffset,
					buttonCenter.y
							+ sin(countLeadRot - PI / 2) * buttonLeadOffset);
			PVector minusPos = new PVector(
					buttonCenter.x
							+ cos(countLeadRot + PI / 2) * buttonLeadOffset,
					buttonCenter.y
							+ sin(countLeadRot + PI / 2) * buttonLeadOffset);

			plus.Update((int) (plusPos.x), (int) (plusPos.y),
					countLeadRot + PI / 2);
			minus.Update((int) (minusPos.x), (int) (minusPos.y),
					countLeadRot + PI / 2);
		}

		public void drawButtons()
		{
			if (plus.isShowing)
				plus.drawButton();
			if (minus.isShowing)
				minus.drawButton();
		}

		public void draw()
		{
			drawButtons();
			drawShadow();
		}

		public void Die()
		{
			super.Die();
			plus.Destroy();
			minus.Destroy();
		}

		public void drawLeads()
		{

			for (Lead l : leads)
			{
				l.draw();
			}

			// This hack is necessary because drawing arcs is way too slow.
			// Drawing the loop as a solid arc brought the framerate to 12 fps
			if (count == 0)
			{
				for (LineOptions line : leads[0].lines)
				{
					if (!line.visible)
						continue;
					stroke(line.col);
					ellipseMode(CENTER);
					ellipse(loopCenter.x, loopCenter.y, line.x_offset * 2,
							line.x_offset * 2);
				}
			}

			// headLoopLead.draw();
		}

		public void drawNumber()
		{

			pushMatrix();
			translate(x_pos, y_pos);
			rotate(leads[0].rotation);
			translate(block_diameter * .8f, 0);
			rotate(PI / 2.0f);

			fill((invertColor ? 0 : 255)); // should match background
			stroke(255);
			strokeWeight(text_size / 10);
			ellipseMode(CENTER);
			// ellipse(0, text_size/10, text_size, text_size);

			textAlign(CENTER, CENTER);
			textSize(text_size);
			fill((invertColor ? 255 : 0)); // text color
			text(displayCount, 0, 0);

			popMatrix();
		}
	}
	/*
	 * RULES OF LOOP LEADS - always has an occupant - block footprint exists in
	 * the middle - has an arc angle, default is 2PI - has a center point -
	 * break distance is anything outside of a certain distance range from the
	 * center (block_diameter/2?) - if a block is placed in the footprint, it
	 * becomes the occupant, former occupant becomes occupant of new occupant's
	 * looplead - if arc angle is less than x, do not draw a footprint
	 */

	public class LoopLead extends Lead
	{
		StartLoopBlock loopBlock;
		Block previous_occupant;

		float ownerAngle;
		float occupantAngle;

		boolean drawFootprint = true;

		LoopLead(Block owner, Block occupant, Block previous,
				StartLoopBlock loopBlock, int index)
		{
			super(owner, 0, index);
			this.occupant = occupant;
			this.loopBlock = loopBlock;
			// loopBlock.loopLeads.add(this);
			loopBlock.blocksInLoop.add(
					loopBlock.blocksInLoop.indexOf(previous) + 1, this.owner);
		}

		LoopLead(Block owner, Block occupant, Block previous,
				StartLoopBlock loopBlock, int index, LeadOptions options)
		{
			this(owner, occupant, previous, loopBlock, index);
			this.options = options;
		}

		public void Update()
		{
			UpdateArcRange();
		}

		public void draw()
		{
			if (!options.visible)
				return;
			colorMode(RGB, 255);

			int numVisible = numLinesVisible();
			int numVisibleVisited = 0;
			float start_radius = loopBlock.loopRadius
					+ (standardWeight * numVisible
							+ lineSeparation * (numVisible - 1)) / 2;

			float arc_range = abs(occupantAngle - ownerAngle);

			pushMatrix();
			translate(loopBlock.loopCenter.x, loopBlock.loopCenter.y);
			rotate(ownerAngle);

			for (int i = 0; i < lines.length; i++)
			{
				LineOptions options = lines[i];
				if (!options.visible)
					continue;

				stroke(options.col);
				strokeWeight(options.weight);
				noFill();

				int offset_radius = (numVisibleVisited
						* (standardWeight + lineSeparation));
				lines[i].x_offset = (start_radius - offset_radius);

				if (options.dashed)
				{
					dashedArc(0, 0, (start_radius - offset_radius), 0,
							arc_range,
							(options.marching ? options.dash_offset : 0));
				}
				else
				{
					// arc(0, 0, (start_radius - offset_radius) * 2,
					// (start_radius - offset_radius) * 2, 0, arc_range);
				}
				numVisibleVisited++;
			}

			if (footprintActive())
			{ // this will draw a footprint

				rotate(arcMiddle());
				translate(loopBlock.loopRadius, 0);
				dashCircle.setStroke(color(255));
				dashCircle.setStrokeWeight(5);
				shapeMode(CENTER);

				shape(dashCircle);
			}

			popMatrix();

			pushMatrix();
			translate(owner.x_pos, owner.y_pos);
			rotate(rotation);

			if (options.image != null)
			{
				pushMatrix();
				translate(distance / 2, 0);
				rotate(PI / 2.0f);
				translate(-options.image.width / 2, -options.image.height / 2);
				fill((invertColor ? 0 : 255));
				noStroke();
				rectMode(CORNER);
				rect(0, 0, options.image.width, options.image.height);

				image(options.image, 0, 0);
				popMatrix();
			}

			if (options.showNumber)
			{

				pushMatrix();
				translate(block_diameter * .8f, 0);
				rotate(PI / 2.0f);

				fill((invertColor ? 0 : 255)); // should match background
				stroke(255);
				strokeWeight(text_size / 10);
				ellipseMode(CENTER);
				ellipse(0, text_size / 10, text_size, text_size);

				textAlign(CENTER, CENTER);
				textSize(text_size);
				fill((invertColor ? 255 : 0)); // text color
				text(options.number, 0, 0);

				popMatrix();
			}

			popMatrix();
		}

		public void highlightTravelled(int lineNum, float percent, int col)
		{
			percent = min(1, percent);
			percent = max(0, percent);
			stroke(col);
			strokeWeight(block_diameter / 4);
			strokeCap(SQUARE);
			noFill();
			pushMatrix();

			translate(loopBlock.loopCenter.x, loopBlock.loopCenter.y);
			// rotate(owner.rotation);
			float arc_range = abs(occupantAngle - ownerAngle);

			arc(0, 0, loopBlock.loopRadius * 2, loopBlock.loopRadius * 2,
					ownerAngle + arc_range * (1.0f - percent), occupantAngle);

			popMatrix();
		}

		public boolean isUnderBlock(Block b)
		{
			if (!footprintActive())
				return false;
			PVector footprintPos = footprintPosition();
			/*
			 * if (debug) { colorMode(RGB); stroke(255, 0, 0); strokeWeight(1);
			 * noFill(); ellipse(footprintPos.x, footprintPos.y, block_diameter
			 * * 1.1, block_diameter * 1.1); }
			 */
			return (dist(footprintPos.x, footprintPos.y, b.x_pos,
					b.y_pos) <= connect_snap_dist);
		}

		public void connect(Block block)
		{
			owner.SetChild(block, leadIndex);
			block.leads[0] = new LoopLead(block, occupant, owner, loopBlock, 0,
					block.leads[0].options);
			block.SetChild(this.occupant, 0);
			this.occupant = block;
		}

		public void disconnect(boolean connectAround)
		{
			if (occupant == owner)
				return;

			// if we want to connect to the block after the current occupant
			if (connectAround && occupant.leads[0].occupant != null)
			{
				// println(owner + " connect around " + occupant + " to " +
				// occupant.leads[0].occupant);
				Block former_occupant = occupant;
				loopBlock.blocksInLoop.remove(occupant);
				owner.SetChild(occupant.leads[0].occupant, 0);
				occupant = occupant.leads[0].occupant;

				former_occupant.RemoveChild(0);
				loopBlock.blocksInLoop.remove(former_occupant);
				former_occupant.leads[0] = new Lead(former_occupant, rotation,
						0, options);

				// println(owner + " child 0 is now " + owner.children[0]);
			}

			else
			{
				// println(owner + " disconnect from child ");
				// owner.parents.get(0).breakConnection(this.owner, true);
			}
		}

		public void trackBlock(Block block)
		{
			float arc_range = abs(occupantAngle - ownerAngle);
			distance = loopBlock.loopRadius * arc_range;

		}

		public boolean footprintActive()
		{
			return abs(occupantAngle - ownerAngle)
					* loopBlock.loopRadius > block_diameter * 2.5f;
		}

		public PVector footprintPosition()
		{
			return convertFromPolar(loopBlock.loopCenter,
					arcMiddle() + ownerAngle, loopBlock.loopRadius);
		}

		public boolean occupantTooFar()
		{
			if (occupant == owner)
				return false;
			float dist = dist(occupant.x_pos, occupant.y_pos,
					loopBlock.loopCenter.x, loopBlock.loopCenter.y);
			return (dist > loopBlock.loopRadius + block_diameter / 2
					|| dist < loopBlock.loopRadius - block_diameter / 2);
		}

		public boolean canRecieveChild()
		{
			return footprintActive();
		}

		public void UpdateArcRange()
		{
			PVector center = loopBlock.loopCenter;
			ownerAngle = atan2((center.y - owner.y_pos),
					(center.x - owner.x_pos));

			ownerAngle = map(ownerAngle, -PI, PI, 0, 2 * PI);

			occupantAngle = atan2((center.y - occupant.y_pos),
					(center.x - occupant.x_pos));

			occupantAngle = map(occupantAngle, -PI, PI, 0, 2 * PI);
			if (occupantAngle <= ownerAngle)
				occupantAngle = occupantAngle + 2 * PI;

			// if(occupant == owner) occupantAngle = ownerAngle + 2*PI;
		}

		public float arcMiddle()
		{
			return (occupantAngle - ownerAngle) / 2;
		}

		public void UpdateRotationFromParent(float rotDelta)
		{
			if (occupant == owner)
			{
				rotation += rotDelta;
			}
		}
	}

	public PShape polygon(float radius, int npoints)
	{
		PShape shape = createShape();
		float angle = TWO_PI / npoints;
		shape.beginShape();
		for (float a = 0; a < TWO_PI; a += angle)
		{
			float sx = cos(a) * radius;
			float sy = sin(a) * radius;
			shape.vertex(sx, sy);
		}
		shape.endShape(CLOSE);
		return shape;
	}

	public PShape dashedCircle(int x, int y, int radius, int numDashes)
	{
		PShape shape = createShape(GROUP);
		noFill();
		stroke(0);
		for (int i = 0; i < numDashes; i++)
		{
			PShape arc = createShape(ARC, x, y, radius, radius,
					(i + .3f) * (2 * PI) / numDashes,
					(i + 1 - .3f) * (2 * PI) / numDashes);
			shape.addChild(arc);
		}
		return shape;
	}

	public void dashedLine(int x1, int y1, int x2, int y2, float offset)
	{
		int lineLength = block_diameter / 4;
		int gapLength = lineLength / 2;

		float distance = dist(x1, y1, x2, y2);
		float rotation = atan2((y2 - y1), (x2 - x1));
		int position = 0;
		strokeCap(ROUND);

		pushMatrix();

		translate(x1, y1);
		rotate(rotation);

		int unitLength = lineLength + gapLength;
		float offsetLength = unitLength * offset;

		float lineOffset = min(lineLength, offsetLength);
		float gapOffset = max(0, (offsetLength - lineLength));

		line(0, 0, lineOffset, 0);
		position += lineOffset;
		translate(lineOffset, 0);

		// stroke(255);
		// line(0, 0, gapOffset, 0);
		position += gapOffset;
		translate(gapOffset, 0);

		while (position < distance)
		{
			line(0, 0, min(distance - position, lineLength), 0);
			position += lineLength;
			translate(lineLength, 0);

			// stroke(255);
			// line(0, 0, gapLength, 0);
			position += gapLength;
			translate(gapLength, 0);
		}

		popMatrix();
	}

	public void dashedArc(int center_x, int center_y, float radius, float start,
			float end, float offset)
	{
		int lineLength = block_diameter / 4;
		// total circumference = 2 * PI * r
		// circumference/lineLength gives us the fraction of 2 PI that results
		// in an arc segment of length lineLength
		float arcAngle = 2 * PI / ((2 * PI * radius) / lineLength);
		float gapAngle = arcAngle / 2;

		float position = start;
		strokeCap(ROUND);
		noFill();
		pushMatrix();
		translate(center_x, center_y);
		// rotate(PI);

		float unitAngle = arcAngle + gapAngle;
		float offsetAngle = unitAngle * offset;

		float gapOffset = min(gapAngle, offsetAngle);
		float arcOffset = max(0, (offsetAngle - gapAngle));

		arc(0, 0, radius * 2, radius * 2, position, position + arcOffset);
		position += arcOffset;
		position += gapOffset;

		while (position < end)
		{
			arc(0, 0, radius * 2, radius * 2, position,
					position + min(end - position, arcAngle));
			// line(0, 0, min(distance-position, lineLength), 0);
			position += arcAngle;
			position += gapAngle;
		}

		popMatrix();
	}

	public PShape sinCircle(int x, int y, int radius, float rotation,
			int numLumps, float amplitude)
	{
		PShape shape = createShape();
		float angle = 0;
		float angleStep = PI / (4 * numLumps);
		float freq = numLumps;
		float amp = amplitude;
		float dx, dy;
		shape.beginShape();

		while (angle <= 2 * PI)
		{
			float localAmp = cos(angle * freq + PI) * amp;
			dx = x + (radius + localAmp) * cos(angle);
			dy = y + (radius + localAmp) * sin(angle);

			angle += angleStep;

			shape.curveVertex(dx, dy);
		}
		shape.endShape(CLOSE);
		return shape;
	}

	public PShape circle(int radius)
	{
		PShape shape = createShape();
		shape.beginShape();
		float angle = 0;

		while (angle <= 2 * PI)
		{
			float dx = cos(angle) * 100;
			float dy = sin(angle) * 100;
			angle += PI / 8;
			shape.curveVertex(dx, dy);
		}
		shape.endShape(CLOSE);
		return shape;
	}

	// TAKES A LOT OF PROCESSING POWER
	public void radialGradient(float x, float y, int radius, int innerColor,
			int outerColor)
	{
		float inter = 1;
		for (int r = radius; r > 0; --r)
		{
			inter = (float) r / (float) radius;
			noStroke();
			fill(lerpColor(outerColor, innerColor, inter));
			ellipse(x, y, r * 2, r * 2);
		}
	}

	public PShape testShape()
	{

		PShape s = createShape();
		s.beginShape();
		s.fill(0, 0, 255);
		s.noStroke();
		s.vertex(0, 0);
		s.vertex(0, 50);
		s.vertex(50, 50);
		s.vertex(50, 0);
		s.endShape(CLOSE);
		return s;
	}

	public class PlayHead
	{
		FunctionBlock origin;
		Block activeBlock;
		LinkedList<Lead> path;
		float pathDecayRate = 1; // pixels/second
		float minDecayDist = 50;
		float pathDist = 0;
		int playColor;
		int lastMillis = 0;
		boolean dead = false;

		Stack<CallBlock> functionCallStack;

		public void Init(FunctionBlock origin, int c)
		{
			path = new LinkedList<Lead>();
			playColor = c;
			allPlayHeads.add(this);
			this.origin = origin;
			origin.spawnedPlayHeads.add(this);
			functionCallStack = new Stack<CallBlock>();
			lastMillis = millis();
		}

		public void Init(FunctionBlock origin, Block start, int c)
		{
			Init(origin, c);
			activeBlock = start;
		}

		// This constructor is for PlayHeads that will highlight the path of a
		// lead that has no occupant, therefore it won't have a start Block
		PlayHead(FunctionBlock origin, int c)
		{
			Init(origin, c);
		}

		PlayHead(FunctionBlock origin, Block start, int c)
		{
			Init(origin, start, c);
			activeBlock.Activate(this, null);
		}

		PlayHead(FunctionBlock origin, Block start, Block previous, int c)
		{
			Init(origin, start, c);
			activeBlock.Activate(this, previous);
		}

		public void Update()
		{
			float deltaTime = (float) (millis() - lastMillis) / 1000;
			pathDist -= max(minDecayDist * deltaTime,
					pathDist * pathDecayRate * deltaTime);
			if (pathDist < 0)
				pathDist = 0;
			if (dead && pathDist <= 1)
			{
				killPlayHeads.add(this);
			}
			lastMillis = millis();
		}

		public void draw()
		{
			highlightPath();
		}

		public void travel()
		{
			Block currentBlock = activeBlock; // activeBlock may change, so we
												// need to keep a reference to
												// it
			boolean hasTravelled = false; // if there are more than 1 valid
											// successors,
			int[] nextBlockIndices = currentBlock.getSuccessors();
			for (int i = 0; i < nextBlockIndices.length; i++)
			{
				int indexOfSuccessor = nextBlockIndices[i];
				Block nextBlock = currentBlock.children[indexOfSuccessor];

				if (nextBlock != null && nextBlock.inChain)
				{ // if there is a block
					if (!hasTravelled)
					{
						addLead(currentBlock.leads[indexOfSuccessor]);
						activeBlock = nextBlock;
						nextBlock.Activate(this, currentBlock);
						hasTravelled = true;
					}
					else
					{// create a new PlayHead that will follow this path
						PlayHead newPlay = new PlayHead(origin, nextBlock,
								currentBlock, playColor);
						newPlay.addLead(currentBlock.leads[indexOfSuccessor]);
					}
				}
				else
				{ // there is no block ahead
					if (functionCallStack.size() > 0)
					{// if there are any CallBlocks in the stack, we'll jump
						// back to the top one
						addLead(currentBlock.leads[indexOfSuccessor]);
						CallBlock callBlock = functionCallStack.pop();
						addLead(callBlock.endLead);
						activeBlock = callBlock;
						activeBlock.Activate(this, currentBlock);
						hasTravelled = true;
					}
					else if (!hasTravelled)
					{// this PlayHead will highlight the dead-end lead and die
						addLead(currentBlock.leads[indexOfSuccessor]);
					}
					else
					{// create a Playhead that will only highlight the dead-end
						// lead and die
						PlayHead newPlay = new PlayHead(origin, playColor);
						newPlay.addLead(currentBlock.leads[indexOfSuccessor]);
					}
				}
			}

			if (!hasTravelled)
			{
				dead = true;
			}
		}

		public void returnToLastFunctionCall()
		{
			if (functionCallStack.size() == 0)
			{
				travel();
			}
			else
			{
				Block currentBlock = activeBlock; // activeBlock may change, so
													// we need to keep a
													// reference to it
				activeBlock = functionCallStack.pop();
				activeBlock.Activate(this, currentBlock);
			}
		}

		// When the playhead jumps from one block to the next, it leaves a thick
		// line that will shrink to catch up
		public void highlightPath()
		{
			// if there are no Leads in the path, there's nothing to do, so
			// return.
			if (path.peekFirst() == null)
				return;

			// first we'll get the sum length of all Leads in the path
			float totalDist = 0;
			for (Lead l : path)
			{
				totalDist += l.distance;// - block_diameter;
			}

			// while there are Leads at the end of path that will no longer be
			// reached given our current pathDist, remove them from path
			while (path.peekFirst() != null
					&& (totalDist - (path.getLast().distance)) > pathDist)
			{
				totalDist = totalDist - (path.getLast().distance);

				path.removeLast();
			}

			float remDist = pathDist;

			for (Lead l : path)
			{
				if (remDist >= (l.distance))
				{
					l.highlightTravelled(origin.sym_id, 1, playColor);
					remDist -= l.distance;
				}
				else
				{
					float percent = (remDist / (l.distance));
					l.highlightTravelled(origin.sym_id, percent, playColor);
				}
			}
		}

		public void addLead(Lead lead)
		{
			path.offerFirst(lead);
			pathDist += lead.distance;// - block_diameter;
		}

		public void addFunctionCall(CallBlock call)
		{
			if (functionCallStack.size() > 0
					&& functionCallStack.peek() == call)
			{
				functionCallStack.pop();
			}
			else
			{
				functionCallStack.push(call);
			}
		}

		public void Die()
		{
			if (activeBlock instanceof SoundBlock)
			{
				((SoundBlock) activeBlock).Stop();
			}
			allPlayHeads.remove(this);
			origin.removePlayHead(this);
		}
	}

	public abstract class SoundBlock extends Block
	{
		boolean isPlaying = false;
		int startTime = 0;
		int playTimer = 0;
		boolean pie = true;

		Block previous;

		AudioPlayer[] clips;

		int activeClip = 0;

		public void Activate(PlayHead play, Block previous)
		{
			if (playHead != null)
			{
				playHead.Die();
			}
			super.Activate(play, previous);
			this.previous = previous;
			Play();
		}

		public boolean isReadyToDie()
		{
			return (true);// !isPlaying); //new surface may have fixed problem
							// of misidentification, if so, this is no longer
							// necessary
		}

		public void LoadClips()
		{
			int clips_id = sym_id < 40 ? sym_id % 6 + 10 : sym_id;
			if (!clipDict.containsKey(clips_id))
			{
				println("No clip found for " + clips_id + ": Possible typo");
				clips_id = 10;
			}
			String[] fileNames = clipDict.get(clips_id);
			clips = new AudioPlayer[fileNames.length];
			for (int i = 0; i < fileNames.length; i++)
			{
				LoadClip(i, fileNames[i]);
			}
		}

		public void LoadClip(int i, String fileName)
		{
			clips[i] = minim.loadFile("clips/" + fileName + ".wav");
			clips[i].rewind();
		}

		public void CloseClips()
		{
			for (AudioPlayer clip : clips)
			{
				clip.close();
			}
		}

		public void SwitchClip(int nextActiveClip)
		{
			if (isPlaying)
			{
				int pos = clips[activeClip].position();
				clips[activeClip].rewind();
				clips[activeClip].pause();
				clips[nextActiveClip].play(pos);
			}
			activeClip = nextActiveClip;
		}

		public void Play()
		{
			playTimer = 0;
			isPlaying = true;
			clips[activeClip].cue(millis() % millisPerBeat);
			clips[activeClip].play();
			startTime = millis();
		}

		public void Stop()
		{
			playTimer = 0;
			isPlaying = false;
			clips[activeClip].rewind();
			clips[activeClip].pause();
		}

		public void Die()
		{
			super.Die();
			CloseClips();
		}
	}

	public class SplitBlock extends Block
	{

		SplitBlock(TuioObject tObj)
		{
			Init(tObj, 2);
		}

		public void Setup()
		{
		}

		public void Update()
		{
			super.Update();
			leadsActive = inChain;
		}

		public void OnRemove()
		{
			super.OnRemove();
		}

		public void Activate(PlayHead play, Block previous)
		{
			super.Activate(play, previous);
			finish();
		}

		// TODO should return two blocks
		public int[] getSuccessors()
		{
			int[] suc = new int[children.length];
			for (int i = 0; i < suc.length; i++)
			{
				suc[i] = i;
			}
			return suc;
		}
	}
	
	public enum BlockType
	{
	    FUNCTION,
	    CLIP,
	    START_LOOP,
	    END_LOOP,
	    CONDITIONAL,
	    EFFECT,
	    BOOLEAN,
	    CALL,
	    SPLIT,
	    BEAT
	    
	};

	public enum TuioAction
	{
	    ADD,
	    REMOVE,
	    UPDATE
	    
	};

	TuioProcessing tuioClient;

	AbstractQueue<TuioActionWrapper> actionQueue = new ConcurrentLinkedQueue<TuioActionWrapper>();
	AbstractQueue<CursorActionWrapper> cursorQueue = new ConcurrentLinkedQueue<CursorActionWrapper>();

	public class TuioActionWrapper
	{
		TuioAction action;
		TuioObject tObject;

		TuioActionWrapper(TuioObject tObj, TuioAction act)
		{
			action = act;
			tObject = tObj;
		}
	}

	public class CursorActionWrapper
	{
		TuioAction action;
		TuioCursor tCursor;

		CursorActionWrapper(TuioCursor tCurs, TuioAction act)
		{
			action = act;
			tCursor = tCurs;
		}
	}

	// these callback methods are called whenever a TUIO event occurs

	// called when an object is added to the scene
	public void addTuioObject(TuioObject tobj)
	{
		if (!isInitiated)
			return;
		actionQueue.offer(new TuioActionWrapper(tobj, TuioAction.ADD));

	}

	// called when an object is removed from the scene
	public void removeTuioObject(TuioObject tobj)
	{
		if (!isInitiated)
			return;

		actionQueue.offer(new TuioActionWrapper(tobj, TuioAction.REMOVE));

	}

	// called when an object is moved
	public void updateTuioObject(TuioObject tobj)
	{
		if (!isInitiated)
			return;

		actionQueue.offer(new TuioActionWrapper(tobj, TuioAction.UPDATE));

	}

	// called when a cursor is added to the scene
	public void addTuioCursor(TuioCursor tcur)
	{
		// if (true) println("add cur "+tcur.getCursorID()+"
		// ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY());

		if (!isInitiated)
			return;
		cursorQueue.offer(new CursorActionWrapper(tcur, TuioAction.ADD));
	}

	// called when a cursor is moved
	public void updateTuioCursor(TuioCursor tcur)
	{
		if (!isInitiated)
			return;

		cursorQueue.offer(new CursorActionWrapper(tcur, TuioAction.UPDATE));
	}

	// called when a cursor is removed from the scene
	public void removeTuioCursor(TuioCursor tcur)
	{
		if (!isInitiated)
			return;

		cursorQueue.offer(new CursorActionWrapper(tcur, TuioAction.REMOVE));
	}

	public void addTuioBlob(TuioBlob tblb)
	{
		if (true)
			println("add blb " + tblb.getBlobID() + " (" + tblb.getSessionID()
					+ ") " + tblb.getX() + " " + tblb.getY() + " "
					+ tblb.getAngle() + " " + tblb.getWidth() + " "
					+ tblb.getHeight() + " " + tblb.getArea());
		// redraw();
	}

	// called when a blob is moved
	public void updateTuioBlob(TuioBlob tblb)
	{
		if (true)
			println("set blb " + tblb.getBlobID() + " (" + tblb.getSessionID()
					+ ") " + tblb.getX() + " " + tblb.getY() + " "
					+ tblb.getAngle() + " " + tblb.getWidth() + " "
					+ tblb.getHeight() + " " + tblb.getArea() + " "
					+ tblb.getMotionSpeed() + " " + tblb.getRotationSpeed()
					+ " " + tblb.getMotionAccel() + " "
					+ tblb.getRotationAccel());
		// redraw()
	}

	// called when a blob is removed from the scene
	public void removeTuioBlob(TuioBlob tblb)
	{
		if (true)
			println("del blb " + tblb.getBlobID() + " (" + tblb.getSessionID()
					+ ")");
		// redraw()
	}

	// called after each message bundle
	// representing the end of an image frame
	public void refresh(TuioTime bundleTime)
	{
		redraw();
	}

	public void TuioUpdate()
	{

		while (actionQueue.peek() != null)
		{
			TuioActionWrapper wrap = actionQueue.poll();
			TuioObject curObj = wrap.tObject;
			switch (wrap.action)
			{
			case ADD:
				if (!checkMissing(curObj))
				{
					Block newBlock = null;
					BlockType type = idToType.get(curObj.getSymbolID());
					if (type == null)
						break; // why is it null sometimes?

					switch (type)
					{
					case FUNCTION:
						newBlock = new FunctionBlock(curObj);
						break;

					case CLIP:
						newBlock = new ClipBlock(curObj);
						break;

					case CONDITIONAL:
						newBlock = new ConditionalBlock(curObj);
						break;

					case EFFECT:
						newBlock = new EffectBlock(curObj);
						break;

					case BOOLEAN:
						newBlock = new BooleanBlock(curObj);
						break;

					case CALL:
						newBlock = new CallBlock(curObj);
						break;

					case SPLIT:
						newBlock = new SplitBlock(curObj);
						break;

					case BEAT:
						newBlock = new BeatBlock(curObj);
						break;

					case START_LOOP:
						newBlock = new StartLoopBlock(curObj);
						break;

					default:
						// newBlock = new FunctionBlock(curObj);
						break;
					}

					//TODO ADD TYPED / SPECIALIZED BLOCK EVENT
					System.out.println("ADDED BLOCK: " + type + " at (" + curObj.getX() + ", " + curObj.getY() + ")");
					
					System.out.println("Appending at ADD BLOCK");
					appendSessionGraph();
				}
				break;

			case REMOVE:

				Block remBlock = blockMap.get(curObj.getSessionID());
				if (remBlock != null)
				{
					remBlock.OnRemove();
					//TODO REMOVE BLOCK EVENT -> THIS ACTUALLY DOESN'T REMOVE THE BLOCK BUT ADDS IT TO MISSING LIST FOR FUTURE GARBAGE COLLECTION IF NOT A MISTAKEN REMOVAL
				}
				break;

			case UPDATE:
				Block upBlock = blockMap.get(curObj.getSessionID());
				if (upBlock != null)
				{
					upBlock.UpdatePosition();
					//TODO UPDATE BLOCK EVENT
					System.out.println("UPDATED BLOCK: " + upBlock.type + " at (" + upBlock.x_pos + ", " + upBlock.y_pos + ")");
					
					System.out.println("Appending at UPDATED BLOCK");
					appendSessionGraph();
				}
				break;
			}
		}

		killRemoved();

		while (cursorQueue.peek() != null)
		{
			CursorActionWrapper wrap = cursorQueue.poll();
			TuioCursor tCur = wrap.tCursor;
			switch (wrap.action)
			{
			case ADD:
				Cursor newCur = new Cursor(tCur);
				break;

			case REMOVE:
				// println(cursorMap.containsKey(tCur.getSessionID()));
				Cursor remCursor = cursorMap.get(tCur.getSessionID());
				if (remCursor != null)
				{

					remCursor.OnRemove();
				}
				break;

			case UPDATE:
				Cursor upCursor = cursorMap.get(tCur.getSessionID());
				if (upCursor != null)
				{
					upCursor.UpdatePosition();
				}
				break;
			}
		}
	}

	public boolean checkMissing(TuioObject tObj)
	{
		for (Block miss : missingBlocks)
		{
			if (dist(miss.x_pos, miss.y_pos, tObj.getScreenX(width),
					tObj.getScreenY(height)) < block_diameter / 2
					&& miss.sym_id == tObj.getSymbolID())
			{ // if this block is close a recently missing block
				miss.find(tObj);
				return true;
			}
		}
		return false;
	}

	public void CheckForExistingTuioObjects()
	{
		for (TuioObject obj : tuioClient.getTuioObjectList())
		{
			if (!blockMap.containsKey(obj.getSessionID()))
			{
				addTuioObject(obj);
			}
		}
	}

	public void Tooltip(String[] info)
	{
		int textSize = 12;
		textSize(textSize);
		int boxHeight = (info.length + 2) * textSize;
		int boxWidth = textSize;

		for (int s = 0; s < info.length; s++)
		{
			boxWidth = max(boxWidth, (int) textWidth(info[s]) + textSize * 2);
		}

		stroke(0);
		strokeWeight(1);
		strokeCap(PROJECT);

		fill(255);
		rectMode(CORNER);
		pushMatrix();
		translate((mouseX + boxWidth >= width ? mouseX - boxWidth : mouseX),
				(mouseY - boxHeight <= 0 ? mouseY : mouseY - boxHeight));
		rect(0, 0, boxWidth, boxHeight);

		textAlign(LEFT, TOP);
		fill(0);

		for (int i = 0; i < info.length; i++)
		{

			translate(0, textSize);
			text(info[i], textSize, 0);
		}

		popMatrix();

	}

	public void SaveData()
	{
		PrintWriter output;

		output = createWriter("logs/" + month() + "-" + day() + "_" + hour()
				+ "_" + minute() + ".txt");

		HashMap<BlockType, Integer> blockCount = new HashMap<BlockType, Integer>();

		/*
		 * for(Chain c : allChains){ for(Block b : c.blocks){ if
		 * (blockCount.containsKey(b.type)) { blockCount.put(b.type,
		 * blockCount.get(b.type)+1); } else{ blockCount.put(b.type, 1); } } }
		 * 
		 * Iterator it = blockCount.entrySet().iterator(); while (it.hasNext())
		 * { Map.Entry pair = (Map.Entry)it.next(); output.println(pair.getKey()
		 * + " " + pair.getValue()); it.remove(); // avoids a
		 * ConcurrentModificationException }
		 */

		output.flush(); // Writes the remaining data to the file
		output.close(); // Finishes the file
	}

	public static PVector convertFromPolar(PVector pos, float rot, float dist)
	{
		return new PVector(pos.x + cos(rot) * dist, pos.y + sin(rot) * dist);
	}

	public Lead lastActiveLeadInChain(FunctionBlock start)
	{
		boolean endFound = false;
		Block currentBlock = start;
		while (!endFound)
		{
			int[] lookNext = currentBlock.getSuccessors();
			if (currentBlock.type == BlockType.START_LOOP)
				lookNext = new int[] { 1 };
			else if (currentBlock.type == BlockType.CALL)
				lookNext = new int[] { 0 };
			if (currentBlock.children[lookNext[0]] != null)
				currentBlock = currentBlock.children[lookNext[0]];
			else
			{
				endFound = true;
				return currentBlock.leads[lookNext[0]];
			}
		}
		return null;
	}

	/**
	 * @return the allBlocks
	 */
	public List<Block> getAllBlocks()
	{
		return allBlocks;
	}

	/**
	 * @param allBlocks the allBlocks to set
	 */
	public void setAllBlocks(List<Block> allBlocks)
	{
		this.allBlocks = allBlocks;
	}

	/**
	 * @return the missingBlocks
	 */
	public List<Block> getMissingBlocks()
	{
		return missingBlocks;
	}

	/**
	 * @param missingBlocks the missingBlocks to set
	 */
	public void setMissingBlocks(List<Block> missingBlocks)
	{
		this.missingBlocks = missingBlocks;
	}
}
