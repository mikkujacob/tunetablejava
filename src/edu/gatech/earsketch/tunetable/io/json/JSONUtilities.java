package edu.gatech.earsketch.tunetable.io.json;

import com.google.gson.Gson;

public class JSONUtilities
{
	private static Gson gson = new Gson();
	
	public static <T> String convertObjectToJSONString(T object, Class<T> typeOfObject)
	{
		try
		{
			String result = gson.toJson(object, typeOfObject);
			return result;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static <T> T convertJSONStringToObject(String JSONString, Class<T> typeOfObject)
	{
		try
		{
			T result = gson.fromJson(JSONString, typeOfObject);
			return result;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
